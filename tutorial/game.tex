\documentclass[11pt,twoside]{ltxdoc}
%%
\usepackage{game}%
\usepackage{showexpl}%
\usepackage[margin=1cm,bottom=2cm,top=2cm,a4paper]{geometry}%
\newcommand\Tikz{Ti{\itshape k}Z/PGF}%
\let\TikZ\Tikz
\lstset{%
  basicstyle=\ttfamily\small,%
  commentstyle=\itshape\ttfamily\small,%
  showspaces=false,%
  showstringspaces=false,%
  breaklines=true,%
  backgroundcolor=\color{lightgray!50!white},%
  breakautoindent=true,%
  captionpos=t,%
  gobble=1%
}%
\setlength{\parskip}{.5ex}%
\setlength{\parindent}{0em}%
\newcommand\tightlist{%
  \setlength{\itemsep}{3pt}%
  \setlength{\parskip}{0pt}}%
\newcommand\copyleftnotice{{%
    \footnotesize%
    \tikz[%
    anchor=center,%
    baseline=($(current bounding box)!.5!(current bounding box.south)$)%
    ]{\wargamelogo[scale=.05]} %
    \copyright{} 2022, Christian Holm Christensen, licensed under %
    \href{http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1}{%
      CC-BY-SA~4.0 %
      \tikz[scale=.25]{\pic[transform shape]{wg/cc by sa};}}}}
\makeatletter%
\def\ps@game{%
  \def\@oddfoot{\hfill\copyleftnotice\hfill}%
  \def\@evenfoot{\hfill\copyleftnotice\hfill}%
  \def\@oddhead{The \textsf{wargame} tutorial\hfill\thepage}%
  \def\@evenhead{\thepage\hfill\rightmark}
  \def\sectionmark##1{%
    \markright{\ifnum\c@secnumdepth>\z@%
      \thesection. \ \fi##1}}}
\makeatother%
\pagestyle{game}%
%
\title{The \LaTeX{} \textsf{wargame} package\\%
  {\large A tutorial}}
\author{Christian Holm Christensen}%
\date{\today}%
%
\def\MacroFont{%
  \fontencoding{\encodingdefault}%
  \fontfamily{\ttdefault}%
  \fontseries{\mddefault}%
  \fontshape{\shapedefault}%
  \footnotesize}
\let\theCodelineNo\empty%
\begin{document}%
\thispagestyle{plain}%
\twocolumn%
\maketitle%

\section{Introduction}

This is a short tutorial on how to use the \LaTeX{} \textsf{wargame}
package.  We will walk through the definition materials for a game.
We will \emph{not} give actual rules for the game.  That is a whole
different topic and I refer you to the literature, for example

\begin{description}\tightlist
\item[\href{https://www.bloomsbury.com/uk/simulating-war-9781472533913/}{\textit{Simulating war}}] by P.Sabin
\item[\href{https://www.professionalwargaming.co.uk/Complete-Wargames-Handbook-Dunnigan.pdf}{\textit{The
      Complete Wargames Handbook}}] by J.F.Dunnigan
\item[\href{https://www.scribd.com/book/220336866/Designing-Wargames-Introduction}{Designing
    Wargames: Introduction}]  by G.Phillies (and associated
  \href{https://www.youtube.com/playlist?list=PLjnA1DE9KX645eBC2gDWHkSlCPvpb4JAQ}{Youtube
    lecture series}) 
  
\end{description}

\section{The game}

The game we will create is a small game with two factions (sides, or
players).   The game is \emph{not} supposed to be play-able, and we'll
leave out a lot of details which we would need in a full game.

\DocInput{game.sty}

\section{Rules}

We will not write any rules for this demonstration game.  The focus of
this tutorial is how to use the \textsf{wargame} package, not how to
design a game.

The \textsf{wargame} package does however have a number of tools that
helps the developer write the rules.  We've already seen some of them
in action.   For example, we saw how to add stacks of units to a board
in Section~\ref{sec:board:units} or how to make board clippings to
show a zoom of the map (Section~\ref{sec:board:clipped}).

Figure~\ref{fig:clipped-attack} showed how we can illustrate an attack,
for example.  The package also provides means to indicate eliminated
units, movement, retreats, advances, and much more.  The manual gives
a lot more information about this.

\DocInput{export.tex}

\section{The materials}

Next, we will make some materials for printing.  The next few pages
will contain only the materials and no text, so we will summarise what
we do here.

The first thing we put out is the map.  That is pretty simple. We do

\begin{lstlisting}
  \begin{board}
  \end{board}
\end{lstlisting}

Then comes the OOB. We simply use the \verb+\fulloob+ macro.

After that are the two tables put into one page.

\begin{lstlisting}
  \crt \tec
\end{lstlisting}

And the final part are the counter sheets.  We will take advantage of
the macros \verb+\alla+ and \verb+\allb+ yet again.

\begin{lstlisting}
  \doublechits{\allb}{4}{0.02}
\end{lstlisting}

As a final bonus, we also make a board that includes our three charts:
The OOB, CRT, and TEC. We will not demonstrate the code here, but take
a look in the sources if you are curious.

\newcommand\fullboard[1][]{%
  \begin{tikzpicture}[#1,rotate=90]%
    \begin{scope}
      \begin{innerboard}
      \end{innerboard}
    \end{scope}
    \begin{scope}[shift={($(frame.north east)+(7,-2.5)$)}]
      \inneroob
    \end{scope}
    %
    \node[
      anchor=south west,
      minimum width=6.2cm,
      minimum height=9.8cm,
      line width=1pt,
      sub title,
      scale=.66,
      draw,
      align=center,
      text depth=9cm,
      fill=neutral,
      transform shape,
      above right=1pt of oob frame.south west
    ] (charts)
    {{\LARGE Charts}};
    %
    \node[above right=5mm and 4mm of charts.south west,
      inner sep=0pt,
      transform shape,scale=.75,anchor=south west] (crt) {\crt};
    %
    \node[above=5mm of crt.north west,anchor=south west,inner sep=0pt,
      transform shape,scale=.9] (tec) {\tec};
    %
    % \end{scope}
    \node[below=1mm of frame.south west,anchor=north west,
      transform shape]{%
        \copyleftnotice};
  \end{tikzpicture}}
  
We put in \verb+\cleardoublepage+ between all this so that we can
print directly on a duplex printer.

\section{Epilogue}

I hope that this short tutorial has helped you. Suggestions,
corrections, and so on are very welcomed.

\begin{flushright}
  \textit{Christian}  
\end{flushright}
\begin{center}
  \begin{tikzpicture}
    \wargamelogo{}
  \end{tikzpicture}
\end{center}

\onecolumn
\cleardoublepage

\begin{center}
  \begin{board}
  \end{board}
\end{center}
\cleardoublepage

\begin{center}
  \fulloob
\end{center}
\cleardoublepage

\begin{center}
  \crt\space\space\tec
\end{center}
\cleardoublepage

\begin{center}
  \begin{tikzpicture}
    \doublechits{\allb}{7}{0.02}
  \end{tikzpicture}

  \begin{tikzpicture}
    \doublechits{\alla}{6}{0.02}
  \end{tikzpicture}
  
  \begin{tikzpicture}
    \doublechits{{game turn chit}}{1}{0.02}
  \end{tikzpicture}
\end{center}

\cleardoublepage
\newgeometry{margin=7mm}
\pagestyle{empty}
\fullboard

\end{document}
%% Local Variables:
%% mode: LaTeX
%% End:
