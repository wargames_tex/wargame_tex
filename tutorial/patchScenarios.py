# --- We may need to import the export module ---
#
from wgexport import *

def patch(build,data,vmod,verbose=False,**kwargs):
    # --- Get the game ---
    game = build.getGame()

    # --- Get the maps --- 
    maps  = game.getMaps()

    # --- Get the main board ---
    board = maps['Board']
    
    # --- Get the mass keys ---
    mkeys = board.getMassKeys()
    mkeys['Eliminate']['icon'] = 'eliminate-icon.png'
    mkeys['Flip']     ['icon'] = 'flip-icon.png'
    
    # --- Get the dead-pool map ---
    pool = maps['DeadMap']
    pool['icon'] = 'pool-icon.png'
    pool.getMassKeys()['Restore']['icon'] = 'restore-icon.png'

    # --- Get the OOB map ---
    oob = game.getChartWindows()['OOBs']
    oob['icon'] = 'oob-icon.png'


    # --- Get our hexagon grid
    grid = board.getBoards()['Board'].getZones()['Hex'].getHexGrids()[0]

    # --- Create a save game
    vsav = vmod.addVSav(build)
    save = vsav.addSaveFile()

    # --- Add unit positions, specified per units 
    save.add(grid,
             **{'a 1 1gh ibn'     : 'C2',
                'a 2 hqbg'        : 'C2',
                'a 1 1lg ibn'     : 'D3',
                'a 2 3gh rbn'     : 'D2',
                'a 1 1 abn'       : 'A1',
                'a aregt'         : 'A1',
                'a 1 hqbg'        : 'A2',
                'a hq'            : 'A2',
                'a 2 1jd abn'     : 'A2',
                'a 1 2jd ibn'     : 'A2'})
    
    # --- Add unit positions, specified per location 
    save.add(grid,
             F6 = ['b hq',
                   'b lg hqregt', 'b lg ibn'],
             E7 = ['b k3 hqregt', 'b k3 31 abibn'],
             D7 = ['b p4 hqregt', 'b p4 41 cabn', 'b p4 42 cabn'],
             D4 = ['b p7 hqregt', 'b p7 71 ibn',  'b p7 72 cabn'],
             D8 = ['b i13 hqregt','b i13 131 ibn','b i13 132 ibn'])

    # with vmod.getInternalFile('Save.vsav','w') as vsave:
    #     # The key is set to 0xAA (alternating ones and zeros)
    #     SaveIO.writeSave(vsave,0xAA,save.getLines())

    # --- Create a file in the module
    vsav.run(description='A save file')

    # --- Add the scenarios 
    scenarios = game.addPredefinedSetup(name = 'Scenarios', isMenu = True)
    scenarios.addPredefinedSetup(name = 'New', useFile=True, file = None)
    scenarios.addPredefinedSetup(name = 'Deployed', file = 'Save.vsav')
#
# EOF
#

