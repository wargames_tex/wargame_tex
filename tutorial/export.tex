% \section{VASSAL module}
% \label{sec:vassal}
%
% As already mentioned several times in this tutorial, we can use the
% definitions of maps, counters, and charts to generate a VASSAL
% module.
%
% \begin{quote}
%   VASSAL\footnote{\url{https://vassalengine.org}} is a
%   cross-platform\footnote{It is written in Java, meaning it runs
%   almost everywhere, except on iOS and Android} tool for playing
%   wargames against other humans or solitaire.  One can do
%   Play-By-E-Mail (PBEM) or online. 
% \end{quote}
%
% The trick is to prepare a separate \LaTeX{} document, say
% \texttt{export.tex} as in this tutorial, which exports the images as
% single pages.  Then, the Python script \texttt{wgexport.py}
% distributed with the \textsf{wargame} package picks up this output
% (\texttt{export.pdf} and \texttt{export.json}) and generates a draft
% VASSAL module.
%
% In addition, you can supply a Python script, say \texttt{patch.py},
% which will be run on the draft module so that you fix things up in
% the VASSAL module.  In this way, your VASSAL module will likely need
% very little hand editing (if at all), and will be robust against
% changes in the \LaTeX{} code\footnote{Assuming you keep your Python
% \texttt{patch.py} script up to date.}.  This of course requires a
% bit of familiarity with Python, which is time well spent.
%
% Let us get into it.
%
% \subsection{The \texttt{export.tex} file}.
% \label{sec:export:tex}
%
% This file will output all the images to a PDF,
% \meta{export}\texttt{.pdf} along with meta information to a
% JSON\footnote{JavaScript Object Notation} file
% \meta{export}\texttt{.json}.  This is relatively simple to do.
%
% We need to use the document class \texttt{wgexport}, so lets go a
% head and load that.
%
%    \begin{macrocode}
\documentclass[11pt]{wgexport}      
%    \end{macrocode}
%
% Make sure you give the right font size as argument.
%
% This is where it pays off that we put all our definitions of
% counters, board, charts, etc. into a separate package.  To import
% these, we simply use that package here\footnote{If we hadn't made
% that package, we would have to repeat the definitions in some
% way. Very error prone}.  So we load the package and start our
% document. 
%
%    \begin{macrocode}
\usepackage{game}
\begin{document}
%    \end{macrocode}
%
% Now the body of the document will simply consist of a list of
% images, one for each side of each counter, the board, charts, and so
% on.  We need to put everything into the environment
% \texttt{imagelist} which will keep track of our meta data.
%
%    \begin{macrocode}
\begin{imagelist}      
%    \end{macrocode}
%
% So far so good.  The first thing we will output are the counters.
% For that we will use the macro \verb+\doublechitimages+ (if we had
% single sided counters, we would use \verb+\chitimages+).  This macro
% takes two arguments: The list of counter definitions and the faction
% of the chits.  Let's go ahead and make those for factions A and B,
% and the special faction ``Markers'' which will only be the game turn
% counter.
%
%    \begin{macrocode}
\doublechitimages[A][chit drop shadows]{\alla}
\doublechitimages[B][chit drop shadows]{\allb}
\doublechitimages[Markers][chit drop shadows]{{game turn chit}}
%    \end{macrocode}
%
% Again, the macros we made for the counters comes in handy.  Note
% that we have used it twice so far: For the OOBs and here for the
% VASSAL module.  We will, in fact, use them one more time to create
% the counter sheet.
%
% To automatically make ``battle markers'' --- markers that identify
% individual battles by placing a numbered marker on top of the
% combatants, we can use the macro \verb+battlemarkers+.  It takes one
% argument, which is the number of unique markers to make and add.
% The markers are round yellow circles with a number in
% them\footnote{This can of course be customised as everything else
% can.}.
%
%    \begin{macrocode}
\tikzset{}
\battlemarkers[marker drop shadows]{12}
\tikzset{every battle marker/.style={}}
%    \end{macrocode}
%
% Furthermore, we can add ``odds'' and battle result markers.   Odds
% markers can be placed via the battle markers context menu, and then
% later be replaced by result markers via the context menu of the odds
% markers.  This is done via the macros \verb+\oddsmarkers+ and
% \verb+\resultmarkers+.  Both macros accept a list of options (odds
% and results, respectively) with a possible background colour to use
% in the markers. 
% 
%    \begin{macrocode}
\oddsmarkers[marker drop shadows]{%
  1:3/red!25!white,%
  1:2/red!15!white,%
  1:1/orange!15!white,%
  2:1/white,%
  3:1/green!15!white,%
  4:1/green!25!white}
\resultmarkers[marker drop shadows]{
  AE/red!50!white,
  AR/red!25!white,
  EX/white,
  DR/green!10!white,
  DE/green!25!white}
%    \end{macrocode}
%
% By default, odds markers must be placed ``by-hand'', but via the
% VASSAL preferences it is possible to calculate the odds and show
% them when the battle is declared.  By default, this calculation will
% only be concerned with the combat factors of the involved units, and
% features such as terrain is ignored.  However, with a bit of Python
% skill, one can flesh out the implementation of the game rules in the
% optional patch (Python) script.  We will not do that here, as a it
% is a little beyond the scope of this tutorial.
%
% Right, so that made our counter images and the associated meta
% data.  Next thing is to add our board.  For that, we use the
% environment \texttt{boardimage}.  Inside that environment we must
% draw the board.  The environment takes one optional argument which
% classifies the board.  Meaningful classifications are \texttt{board}
% (default) and \texttt{oob} for OOBs.  The first mandatory argument
% is the name of the board (more or less free form text, except any of 
% ``\texttt{./\textbackslash '"}'' should not be used.  The second
% mandatory argument is a sub-category, and is mainly reserved for
% future use.
%
%    \begin{macrocode}
\begin{boardimage}{Board}{}
  \begin{board}
  \end{board}
\end{boardimage}
%    \end{macrocode}
%
% We will make another board image for our OOB, since that component
% should hold counters in VASSAL. We pass the optional argument
% \texttt{oob} in this case. 
%
%    \begin{macrocode}
\begin{boardimage}[oob]{OOB}{}%
  \fulloob
\end{boardimage}
%    \end{macrocode}
%    
% We have two charts that we would also like to be put in: The CRT and
% the TEC.  These \emph{must} be put into \Tikz{} pictures, and we
% precede those with the macro \verb+\info+. This macro takes three
% arguments: The name of the next image, the category, and
% sub-category.   Again, the name is more or less free form, and the
% category in some sense dictates how it will present in the VASSAL
% module.  For charts we should use the category \texttt{chart}.  The
% sub-category isn't really used at this point.  
%
%    \begin{macrocode}
\info{CRT}{chart}{}
\begin{tikzpicture}
  \node{\crt};
\end{tikzpicture}
% 
\info{TEC}{chart}{}
\begin{tikzpicture}
   \node{\tec};
\end{tikzpicture}
%    \end{macrocode}
%
% We also want background images for our pools of eliminated units in
% VASSAL.  This will be enlarged version of main headquarter unit of
% each faction, overlayed with a white semi-transparent background.
% 
%    \begin{macrocode}
\info{A}{pool}{}
\begin{tikzpicture}[scale=7]
  \chit[a hq];
  \fill[white,opacity=.5](-.6,-.6)rectangle++(1.2,1.2);
\end{tikzpicture}
\info{B}{pool}{}
\begin{tikzpicture}[scale=7]
  \chit[b hq];
  \fill[white,opacity=.5](-.6,-.6)rectangle++(1.2,1.2);
\end{tikzpicture}
%    \end{macrocode}
%    
% We can add splash page image.  We have not defined such an image in
% the \texttt{game} package, but we could have, so we will make it
% here. Again, it should be a \Tikz{} picture. For this, we need to
% use the category \texttt{front}.
%
%    \begin{macrocode}
\info{Splash}{front}{}
\begin{tikzpicture}
  \node[board frame,title,
  minimum width=5cm,minimum height=5cm]
  {A Game};
\end{tikzpicture}
%    \end{macrocode}
%
% We could continue to  add more if we wanted to.  Basically anything
% can be added.  For example, one might want custom button icons or
% the like.   We will add a few such icons here, mainly to show how
% the \texttt{patch.py} script works.
%
% The \textsf{wgexport.cls} class provides a number of icons we may
% use for this.  These are
%
% \begin{center}
%   \begin{tikzpicture}
%     \pic            {flip icon};
%     \pic at (2.0,0) {restore icon};
%     \pic at (3.5,0) {eliminate icon};
%     \pic at (5.0,0) {pool icon};
%     \pic at (6.5,0) {oob icon={black}{black}};
%   \end{tikzpicture}
% \end{center}
% 
%
% We will add these as pictures to our export PDF. First, the image
% for the pool of units. 
%
%    \begin{macrocode}
\info{pool-icon}{icon}{}
\begin{tikzpicture}[transform shape,scale=.4]
  \pic{pool icon};
\end{tikzpicture}
%    \end{macrocode}
%
% The category \texttt{icon} has no special meaning.  
%
% We also want to add a custom icon for the OOB button.  VASSAL has a
% button icon for the piece inventory which is really quite
% appropriate, but alas it is already in use, so we will make our
% own. Here, we use the \TikZ{} picture \texttt{oob icon} provided by
% \textsf{wgexport}.  This picture needs two arguments: the left hand
% and right hand sides fill colours.  We will use our background
% colours. 
%
%    \begin{macrocode}
\info{oob-icon}{icon}{}
\begin{tikzpicture}[transform shape,scale=.4]
  \pic{oob icon={a-bg}{b-bg}};
\end{tikzpicture}
%    \end{macrocode}
%   
% Normally the \texttt{wgexport.py} script uses the \emph{undo} image
% for the flip button.  However, that may be a bit confusing, so we
% will use a custom image for that.  We will use the pictures provided
% by \textsf{wgexport}.
%
%
%    \begin{macrocode}
\info{flip-icon}{icon}{}
\begin{tikzpicture}[transform shape,scale=.4]
  \pic{flip icon};
\end{tikzpicture}
%
\info{eliminate-icon}{icon}{}
\begin{tikzpicture}[transform shape,scale=.4]
  \pic{eliminate icon};
\end{tikzpicture}
%
\info{restore-icon}{icon}{}
\begin{tikzpicture}[transform shape,scale=.4]
  \pic{restore icon};
\end{tikzpicture}
%    \end{macrocode}
%
%    \begin{macrocode}
\end{imagelist}
\end{document}
%    \end{macrocode}
%
% \subsection{Make the draft module}
%
% We run \LaTeX{} on the above \texttt{export.tex} file (the source of
% this section) to generate \texttt{export.pdf} and
% \texttt{export.json}.  We then process these with the
% \texttt{wgexport.py} Python script to get the draft VASSAL module
% \texttt{Draft.vmod}.
%
% \begin{verbatim}
% `kpsewhich wgexport.py` export.pdf export.json 
% \end{verbatim}
%
% The utility \texttt{kpsewhich}\footnote{\TeX{}Live and similar
% \TeX{} distributions.} will report the location of a file in the
% \TeX{} installation tree(s).  Note that the \texttt{`} above are
% what is typically called `back-ticks' (i.e., what you typically put
% as the leading character of scare-crows in \LaTeX{}).  This is of
% course Un*x-like syntax.  For other OSs, consult your \TeX{}
% distribution's documentation for how to find files in the \TeX{}
% installation.
%
% You can open the module in the VASSAL editor and see what was
% generated.  On the board, all areas marked with the \texttt{zone
% scope} (or \texttt{zone path}) should be defined as zones.  In the
% OOB, we should have two zones, one for the A faction and one for the
% B faction.   We should also have three groups of counters --- one
% for each faction and one for ``Markers''. Finally, we should have a
% window with tabs, one for each defined chart. 
%
% We can add the rules to the module.  So suppose the rules are in
% \texttt{game.pdf}, then we can do 
%
% \begin{verbatim}
% `kpsewhich wgexport.py` export.pdf export.json \
%    -r game.pdf -o Game.vmod
% \end{verbatim}
%
% to add the rules to the ``Help'' menu.  Other information is added
% to that menu too, such as key-bindings and a short note on how the
% module was generated.  Of course, you should also see the splash
% image that we defined.  The option \texttt{-o}~\meta{filename}
% writes the module to \meta{filename} rather than the default
% \texttt{Draft.vmod}. 
%
% We define the version of the module by passing the option
% \texttt{-v}~\meta{version} to the Python script.  By default, the
% version is set to \texttt{draft}, and a note on the draft is added
% to the module and the grids are drawn.   Any other version string
% will suppress these.
%
% Also, the default module name is \texttt{Draft} and a similar
% description is likewise the default.  We can change this by passing
% the options \texttt{-t}~\meta{title} and
% \texttt{-d}~\meta{description}.  Note, you may want to quote the
% arguments if they contain spaces or the like.  Thus, to make a more
% complete module, we would do something like
%
% \begin{verbatim}
% `kpsewhich wgexport.py` export.pdf export.json \
%   -r game.pdf -o Game.vmod \
%   -t "LaTeX Wargame tutorial" \
%   -d "Example of using wargame to make a module" \
%   -v 0.1
% \end{verbatim}
%
% Note that you typically need to quote (\texttt{"}) longer strings
% that contain spaces and other special characters. 
%
% \subsection{Going the extra mile}
% \label{sec:vassal:patch}
%
% We will take one more step on the VASSAL module, and then we will
% get back to the Print'n'Play version of the game.  We can provide
% the \texttt{wgexport.py} script with an additional Python script that
% can tweak the VASSAL module any way we like.  We can for example
% move counters into their starting positions, to the OOB, adjust
% grids, add more materials, and so on.  It is only your imagination,
% and Python programming skills, that sets the limits on what you can
% do.
%
% Here we will make a simple \texttt{patch.py} Python script which
% does very little.  The \textsf{wargame} package,
% \textsf{wgexport} class, and \texttt{wgexport.py} script  has already
% done the heavy work for us. Not least be because we have taken care
% to add in \texttt{zone} styles where needed. 
%
% We will put all the counters on the OOB chart, and adjust some grids
% - most notably the OOB and turn track grids.  Other than that, we
% will not do much.
% 
% \begin{verbatim}
% `kpsewhich wgexport.py` export.pdf export.json \
%   -r game.pdf -o Game.vmod
%   -t "LaTeX Wargame tutorial" \
%   -d "Example of using wargame to make a module" \
%   -v 0.1 -p patch.py
% \end{verbatim}
%
% Now for the script:
%
% \begin{lstlisting}[language=Python]
% # --- We may need to import the export module ---
% #
% # from wgexport import *
% 
% def patch(build,data,vmod,verbose=False,**kwargs):
%     # --- Get the game ---
%     game = build.getGame()
% 
%     # --- Get the maps --- 
%     maps  = game.getMaps()
% 
%     # --- Get the main board ---
%     board = maps['Board']
%     
%     # --- Get the mass keys ---
%     mkeys = board.getMassKeys()
%     mkeys['Eliminate']['icon'] = 'eliminiate-icon.png'
%     mkeys['Flip']     ['icon'] = 'flip-icon.png'
%     
%     # --- Get the dead-pool map ---
%     pool = maps['DeadMap']
%     pool['icon'] = 'pool-icon.png'
%     pool.getMassKeys()['Restore']['icon'] = 'restore-icon.png'
% 
%     # --- Get the OOB map ---
%     oob = game.getChartWindows()['OOBs']
%     oob['icon'] = 'oob-icon.png'
% 
% 
% #
% # EOF
% #
% \end{lstlisting}
%
% In the \texttt{patch.py} script we can use all the functionality
% provided by \texttt{wgexport.py}.  Elements are XML elements that we
% apply \texttt{xml.dom.minidom} operations on.
%
% Note, if we need to use classes, etc.\ from \texttt{wgexport.py},
% then we ought to import that module into our patch script, as shown
% in the top comment above.  In the example patch script we do not
% need that, so we leave it out.
%
% One can do quite complicated things in VASSAL, which can be set-up
% in the patch script.  By default, the \texttt{wgexport} script sets
% up the pieces and boards, and if one has defined regions with
% specific piece names, then the script will likewise place the pieces
% there (as with our \texttt{game turn} example above).
%
% One can, for example, setup the module so that the game turn marker
% automatically flips or progresses when the turn track widget reach
% specific points.  One can in principle also set it up so that when a
% specific turn or phase is reach, then pieces are moved from the OOB
% to the board.  The more one can do like that, the more the game is
% automated. 
% 
% \iffalse
% Local Variables:
%   mode: docTeX
%   TeX-master: "game.tex"
% End:
% \fi
