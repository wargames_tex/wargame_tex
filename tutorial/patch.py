# --- We may need to import the export module ---
#
# from wgexport import *

def patch(build,data,vmod,verbose=False,**kwargs):
    # --- Get the game ---
    game = build.getGame()

    # --- Get the maps --- 
    maps  = game.getMaps()

    # --- Get the main board ---
    board = maps['Board']
    
    # --- Get the mass keys ---
    mkeys = board.getMassKeys()
    mkeys['Eliminate']['icon'] = 'eliminate-icon.png'
    mkeys['Flip']     ['icon'] = 'flip-icon.png'
    
    # --- Get the dead-pool map ---
    pool = maps['DeadMap']
    pool['icon'] = 'pool-icon.png'
    pool.getMassKeys()['Restore']['icon'] = 'restore-icon.png'

    # --- Get the OOB map ---
    oob = game.getChartWindows()['OOBs']
    oob['icon'] = 'oob-icon.png'


#
# EOF
#

