# A package to make Hex'n'Counter wargames in LaTeX

This package can help make classic [Hex'n'Counter wargames][1] using
LaTeX. The package provide tools for generating

- Hex maps and boards 
- Counters (called _chits_ since TeX have already taken _counters_)
  for units, markers, and so on
- Counter sheets 
- Order of Battle charts 
- Illustrations in the rules using the defined maps and counters

The result will often be a PDF (or set of PDFs) that contain
everything one will need for a game (rules, charts, boards, counter
sheets). 

- The package uses [NATO App6](#nato-app6 'Section below') symbology for
  units.
- The package uses [TikZ][2] for most things. 
- The package support exporting the game to a [VASSAL](#vassal-support
  'Section below') module

## Sources 

The sources of the package are kept at [GitLab][3]

## Downloads available at GitLab 

- [Zip file of package and support files][wargame_tex.zip]
- [Browse content of package][browse]
- [Documentation][wargame.pdf]
- [Tutorial][tutorial.pdf] (and associated VASSAL [module][Game.vmod])
- [Table of symbols][symbols.pdf]
- [Compatibility][compat.pdf]

## Installation 

The instructions below are geared towards Un*x-like systems, for
example GNU/Linux and MacOSX.  For more information on setting the
prerequisites and this package on various platforms, please see [this
page](https://wargames_tex.gitlab.io/wargame_www/build.html).

### From ZIP file 

To install, get the ([zip file][wargame_tex.zip]) and unzip into your
TeX tree, for example 

	mkdir -p ~/texmf
	(cd ~/texmf && unzip ../wargame_tex.zip)
	
### From git clone 

If you clone from GitLab 

    git clone https://gitlab.com/wargames_tex/wargame_tex.git
    
to get the sources, then you can do, 

    cd wargame
	make install 
	
to install in `~/texmf`.  If you prefer another destination, say
`/usr/local/share/texmf`, do 

	make install DESTDIR=/usr/local/share/texmf

## Download from CTAN 

The package is available from [CTAN][4] in the directory
`/macros/latex/contrib/wargame`.  The package is part of the CTAN
distribution [TeXLive][5].

### From TDS zip archive 

You can get the *TDS* [zip
file](https://mirrors.ctan.org/install/macros/latex/contrib/wargame.tds.zip)
and unpack that into your desired destination, e.g., `~/texmf` 

	unzip wargame.tds.zip -d ~/texmf
    
or 

    sudo unzip wargame.tds.zip -d /usr/local/share/texmf 
    
### From sources

If you get the source (`wargame.ins`, all `.dtx`,`.py`, and
`Makefile`) files, then do

	make install 
	
to install into `~/texmf`.  If you prefer to install somewhere else,
say `/usr/local/share/texmf`, then do 

	make install DESTDIR=/usr/local/share/texmf 
	
If you do not have `make` (or `gmake` on MacOSX), or because you are
on some system which does not have that tool (e.g., Windows), then do
(the equivalent of)

	latex wargame.ins
    pdflatex wargame.beach
    pdflatex wargame.city
	pdflatex wargame.light_woods
    pdflatex wargame.mountains
    pdflatex wargame.rough
    pdflatex wargame.swamp
    pdflatex wargame.town
    pdflatex wargame.village
    pdflatex wargame.woods

(You need to use `pdflatex`, `xelatex`, or `lualatex` - plain `latex`
with DVI output will not work)

Then copy the relevant files to your TeX tree (e.g., `~/texmf/`)
as 

    mkdir ~/texmf/tex/latex/wargame
	cp tikzlibrary*.tex ~/texmf/tex/latex/wargame/
	cp wargame.sty      ~/texmf/tex/latex/wargame/
	cp wgexport.cls     ~/texmf/tex/latex/wargame/
    cp wargame.*.pdf    ~/texmf/tex/latex/wargame/
	cp wgexport.py      ~/texmf/tex/latex/wargame/
	cp wgsvg2tikz.py    ~/texmf/tex/latex/wargame/
	
To generate the documentation, after having done the above, do 

	pdflatex wargame.dtx
	makeindex -s gind -o wargame.ind wargame.idx
	pdflatex wargame.dtx
	pdflatex wargame.dtx
	pdflatex symbols.tex
	pdflatex compat.tex
	pdflatex compat.tex
	
(You need to use `pdflatex`, `xelatex`, or `lualatex` - plain `latex`
with DVI output will not work)

You can install these into your TeX tree with  (e.g., `~/texmf/`)

	mkdir -p ~/texmf/doc/latex/wargame/
    cp wargame.pdf  ~/texmf/doc/latex/wargame/
	cp symbols.pdf  ~/texmf/doc/latex/wargame/
	cp compat.pdf   ~/texmf/doc/latex/wargame/
    
If you want to generate the tutorial document, do 

	cd tutorial 
	make 
	
or 

	cd tutorial
	pdflatex game
	pdflatex game 
	
If you also want to make the tutorial VASSAL module, do (also in
`tutorial`), do 

	pdflatex export.tex 
	../wgexport.py export.pdf export.json -o Game.vmod \
		-d "Example module from LaTeX PnP game" \
		-t "LaTeX wargame tutorial" -v 0.1 \
		-p patch.py
		
Note, you need `pdfinfo` and `pdftocairo` from Poppler, and Python
with the module `PIL`  for this.  On Debian-based systems, do 

	sudo apt install poppler-utils python3-pil


## Tutorial 

See the [tutorial](tutorial/README.md 'Link works on GitLab only')
page for more
([here](https://ctan.org/tex-archive/macros/latex/contrib/wargame/doc/tutorial/)
if you browse from CTAN). 

Another simple example to get you started is the game
[Tannenberg4][advancedguard_tex] also available from GitLab. 

## Examples 

Below are some print'n'play board wargames made with this package.
See also [LaTeX Wargames][wargames_www] for more.  These are not
original games but rather revamps of existing games.  All credits goes
to the original authors of these games.

- Napoleonic 
  - [Napoleon at Waterloo][naw_tex] (SPI)
- American Civil War 
  - [Smithsonian Gettysberg][sgb_tex] (Avalon Hill)
- WWI
  - [Tannenberg4][advancedguard_tex]
- WWII 
  - Eastern Front 
    - [Battle for Moscow][bfm_tex] (classic introductory game)
  - Western Front
	- [D-Day][dday_tex] (Avalon Hill classic)
	- [Smithsonian D-Day][sdday_tex] (Avalon Hill)
	- [Smithsonian Battle of the Bulge][sbotb_tex] (Avalon Hill)
    - [The Drive on Metz][dom_tex] (from _The Complete Wargames Handbook_)
  - Pacific theatre 
    - [First Blood][firstblood_tex] (Avalon Hill International
      Kriegspiel Association classic)
  - Africa 
    - [Afrika Korps][afrikakorps_tex] (Avalon Hill classic)
    - [Panzerarmee Afrika][paa_tex] (SPI and Avalon Hill)
- Modern/Speculative
  - [Strike Force One][sfo_tex] (SPI)
  - [Kriegspiel][kriegspiel_tex] (Avalon Hill classic)
  - [Port Stanley][portstanley_tex] (from the _Wargamer_ magazine)
- Abstract 
  - [Battle][battle_tex]

## VASSAL support

The packages has the script [`wgexport.py`](utils/wgexport.py) to
generate a draft [VASSAL][6] module from the defined charts, boards,
counters, OOBs, and so on.  More about this is given in the
documentation. 

The script will generate a first draft of a module, which then can be
edited in the VASSAL editor for the final thing. 

Alternatively, one can provide a Python script to do more on the
generated module, such as defining starting positions, fixing grid
layouts, and so on.  Using such a patch script, one can get an
(almost) final module.  This means, that even if one makes changes to
the original content, it is easy to remake the module without much
need for the VASSAL editor. 

An example of this is given in the [Battle for Moscow][bfm_tex]
project, and of course in the [tutorial](tutorial/ 'Link works on
GitLab only')

The sources of this script is kept in a different project
[`pywargame`][pywargame] also on GitLab.  The script is generated in
that package.  The `pywargame` package allows one to manipulate VASSAL
modules from Python, or to read (and convert) [CyberBoard][cyberboard]
([GitHub][cyberboard_git]) scenarios.

## NATO App6 

The package supports 

- All air, equipment, installation, land, sea surface, sub surface,
  and space command symbols, including amplifiers and modifiers.
- All friendly, hostile, neutral, and unknown faction frame.
  Undecided faction frames can be made by specifying `dashed` line
  styles. 
- Some, but very few, other kinds of symbology. 

What is _not_ supported 

- Everything but what is listed above.  This includes 
  - Cyber command frame (similar to land, but has a filled triangle in
    the corner) 
  - Dismounted individuals (similar to land, except for the friendly
    faction where the frame is a hexagon)
  - Some of the newer symbols in revision _E_ of the standard
    (relative to revision _D_), mainly in the cyper section.
  - Control measures symbols 

Here are some references for more information on the NATO App6
symbology standard. 

- The [Wikipedia page][7] on NATO Joint Military Symbology
- The [NATO page][8] (latest revision is _E_).  If this does not work
  for you, then try to go to the [standards page][9] and put in and
  write in `SYMBOLOGY` in the _Document Title Contains Words_ field
  and press _Start_.
- Other LaTeX package for making NATO symbology [XMilSymb][10]

## Copyright and license

(c) 2022 Christian Holm Christensen 

This work is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License (CC-BY-SA-4.0). To
view a copy of this license, visit [CC][11] or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

### Some comments on copyright 

_Caveat_: I am not a lawyer. 

Note that this license applies to the `wargame` package as such.  What
you generate using the package is _not_ required to be licensed
according to the _ShareAlike_ clause of CC-BY-SA-4.0.  That is, you
can license your materials _any way you want_.

However, copyright rights on games is not as prohibitive as you
may think (see [this thread on BGG][12]).  What you _can_ copyright is
original, artistic expression.  That is, the copyrighted work must not
be a copy of something else (originality) and must be some form of
expression.  One _cannot_ copyright ideas, only their expression in so
far as it is _artistic_ (i.e., not a trivial expression that anyone
knowledgeable within the field can do with rudimentary effort).

This means you _can not_ copyright your game mechanics, for example,
only how you described them.  You _can not_ copyright a title (but you
may be able to claim trademark on it).  You _can_ copyright the
wording of the rules, the graphics that you use, and so on.

This also means, that you are essentially free to make your own
version of previously published game, _as long as_ 

- you do not copy existing text, 
- you do not copy existing graphics, and 
- you respect any kind of trademark claims

However, it is advisable to contact the copyright holders of the
previously published game to avoid [SNAFU][13].  If in doubt, seek
professional help. 

[1]:  https://en.wikipedia.org/wiki/Wargame
[2]:  https://github.com/pgf-tikz/pgf
[3]:  https://gitlab.com/wargames_tex/wargame_tex
[4]:  https://ctan.org/tex-archive/macros/latex/contrib/wargame
[5]:  https://tug.org/texlive/index.html
[6]:  https://vassalengine.org
[7]:  https://en.wikipedia.org/wiki/NATO_Joint_Military_Symbology 
[8]:  https://nso.nato.int/nso/nsdd/main/standards/ap-details/3169/EN
[9]:  https://nso.nato.int/nso/nsdd/main/standards
[10]: https://github.com/ralphieraccoon/MilSymb
[11]: http://creativecommons.org/licenses/by-sa/4.0/
[12]: https://boardgamegeek.com/thread/493249/
[13]: https://en.wikipedia.org/wiki/SNAFU
    
	
[wargame_tex.zip]: https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/download?job=dist
[browse]: https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/browse?job=dist	
[wargame.pdf]: https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/doc/latex/wargame/wargame.pdf?job=dist
[tutorial.pdf]: https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/doc/latex/wargame/tutorial.pdf?job=dist
[Game.vmod]: https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/doc/latex/wargame/Game.vmod?job=dist
[symbols.pdf]: https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/doc/latex/wargame/symbols.pdf?job=dist
[compat.pdf]: https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/doc/latex/wargame/compat.pdf?job=dist

[wargames_www]:      https://wargames_tex.gitlab.io/wargame_www/
[advancedguard_tex]: https://gitlab.com/wargames_tex/advancedguard_tex
[battle_tex]:        https://gitlab.com/wargames_tex/battle_tex
[bfm_tex]:           https://gitlab.com/wargames_tex/bfm_tex
[hellsgate_tex]:     https://gitlab.com/wargames_tex/hellsgate_tex
[unternehmung_tex]:  https://gitlab.com/wargames_tex/unternehmung_tex
[dday_tex]:          https://gitlab.com/wargames_tex/dday_tex
[sdday_tex]:         https://gitlab.com/wargames_tex/sdday_tex
[sbotb_tex]:         https://gitlab.com/wargames_tex/sbotb_tex
[sgb_tex]:           https://gitlab.com/wargames_tex/sgb_tex
[dom_tex]:           https://gitlab.com/wargames_tex/dom_tex
[pkmg_tex]:          https://gitlab.com/wargames_tex/pkmg_tex
[pkdday_tex]:        https://gitlab.com/wargames_tex/pkdday_tex
[firstblood_tex]:    https://gitlab.com/wargames_tex/firstblood_tex
[malaya_tex]:        https://gitlab.com/wargames_tex/malaya_tex
[afrikakorps_tex]:   https://gitlab.com/wargames_tex/afrikakorps_tex
[boel_tex]:          https://gitlab.com/wargames_tex/boel_tex
[sfo_tex]:           https://gitlab.com/wargames_tex/sfo_tex
[kriegspiel_tex]:    https://gitlab.com/wargames_tex/kriegspiel_tex
[portstanley_tex]:   https://gitlab.com/wargames_tex/portstanley_tex
[paa_tex]:           https://gitlab.com/wargames_tex/paa_tex
[naw_tex]:           https://gitlab.com/wargames_tex/naw_tex
[pywargame]:         https://gitlab.com/wargames_tex/pywargame
[cyberboard]:        http://cyberboard.norsesoft.com/
[cyberboard_git]:    https://github.com/CyberBoardPBEM/cbwindows
[pillow]: 	         https://pillow.readthedocs.io/en/stable/
[texshop]:           https://pages.uoregon.edu/koch/texshop/
[texworks]:          https://www.tug.org/texworks/

