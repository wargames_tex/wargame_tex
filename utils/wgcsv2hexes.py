#!/usr/bin/env python
# -*- python -*-
'''Script to generate LaTeX code from a CSV, Spreadsheet, or JSON file

This script takes an input Comma-Separated-Values (CSV) or Spreadsheet
(LibreOffice or Excel), or Java Serialized Object Notation (JSON)
file, and generates LaTeX (using the package `wargame`) code that will
typeset counter sheets.

Each entry in the CSV, Spreadheet, or JSON can have the following
entries (columns in CSV and Spreadsheet):

- `Column`:          The column number or letter(s)
- `Row`:             The row number of letter(s)
- `Terrain`:         Terrain of hex (possibly list)
- `Clip`:            Terrain clip
- `Label`:           Hex label
- `Town`:            Town
- `Extra`:           Extra stuff
- `Draw`:            Draw colour
- `Fill`:            Fill colour

All columns, except `Column` and `Row` are optional.

Please also refer to the `wargame` documentation at

    https://gitlab.com/wargames_tex/wargame_tex

for more.
'''
# --------------------------------------------------------------------
def read_specs(file_or_name='units.csv',sheet=0):
    from pathlib import Path
    from sys import stderr
    
    if isinstance(file_or_name,str):
        input = open(file_or_name,'r')
    else:
        input = file_or_name

    name = input.name
    ext  = Path(name).suffix

    meth = {'.csv':  read_csv,         
            '.ods':  read_spreadsheet, 
            '.odf':  read_spreadsheet, 
            '.odt':  read_spreadsheet, 
            '.xls':  read_spreadsheet, 
            '.xlsx': read_spreadsheet, 
            '.xlsm': read_spreadsheet, 
            '.xlsb': read_spreadsheet,
            '.json': read_json,
            }.get(ext,None)

    if meth is None:
        raise RuntimeError(f'Don\'t know how to deal with '
                           f'"{name}"',file=stderr)
    
    l = meth(input,sheet=sheet)

    if isinstance(file_or_name,str):
        input.close()

    return l

    
# --------------------------------------------------------------------
def read_csv(input,**kwargs):
    from csv import DictReader
    
    reader = DictReader(input)
     
    return list(reader)

# --------------------------------------------------------------------
def read_json(input,**kwargs):
    from json import load

    return load(input)

# --------------------------------------------------------------------
def read_spreadsheet(input,sheet=0,**kwargs):
    try:
        from pandas import read_excel

        try:
            sheet = int(sheet)
        except:
            pass
        
        pd = read_excel(input.name,sheet)
        return pd.to_dict('records')
    
    except ImportError as e:
        raise RuntimeError('Pandas not installed') from e
    except:
        raise

# --------------------------------------------------------------------
def get_field(entry,name):
    from math import isnan 
    field = entry.get(name, '')
    if field == '':
        return field

    if isinstance(field,int) and isnan(field):
        return ''

    if isinstance(field,float):
        if isnan(field):
            return ''
        if round(field) == field:
            return str(int(field))
        
    return str(field).strip()

# --------------------------------------------------------------------
def format_coord(s):
    try:
        return int(s)
    except:
        pass

    c2n = lambda c: ord(c)-ord('A')+1
    b   = c2n('Z')
    c2l = lambda c: [c2n(cc) for i,cc in enumerate(c)]
    l2n = lambda l: [n * b**i for i,n in enumerate(l[::-1])]

    return sum(l2n(c2l(s)))
    
# --------------------------------------------------------------------
def format_hex(entry,oldc,oldr):
    from textwrap import dedent as d, indent as i

    column  = get_field(entry,'Column')
    row     = get_field(entry,'Row')
    terrain = get_field(entry,'Terrain')
    clip    = get_field(entry,'Clip')
    extra   = get_field(entry,'Extra')
    label   = get_field(entry,'Label')
    fill    = get_field(entry,'Fill')
    draw    = get_field(entry,'Draw')
    town    = get_field(entry,'Town')
    more    = get_field(entry,'More')
    
    if column == '' and row == '':
        return None,None,None

    if column == '': column = oldc
    if row    == '': row    = oldr

    if column == '' and row == '':
        return None,None,None

    column = format_coord(column)
    row    = format_coord(row)

    coords  = f'(c={column:2d},r={row:2d})'
    terr    = ''
    if terrain != '':
        cl = ''
        if clip != '':
            cl = f',clip={{{clip}}}'
        terr = f'terrain={{{terrain}{cl}}}'

    lab    = f'label={{{label}}}' if label != '' else ''
    ext    = f'extra={{{extra}}}' if extra != '' else ''
    fil    = f'fill={fill}' if fill != '' else ''
    drw    = f'draw={draw}' if draw != '' else ''
    twn    = f'town={{{town}}}' if town != '' else ''
    mre    = f'{more}' if more != '' else ''
    opt    = ','.join([terr,lab,twn,ext,fil,drw,mre])
    
    return column,row,fr'\hex[{opt}]{coords};'
    
# --------------------------------------------------------------------
def format_hexes(hexes):
    from textwrap import dedent as d, indent as i

    oldc = ''
    oldr = ''
    db   = []
    for h in hexes:
        oldc, oldr, c = format_hex(h,oldc,oldr)
        if not oldc or not oldr:
            continue
        
        db.append((oldc,oldr,c))
        
    return db
        
# --------------------------------------------------------------------
def format_comment(args,defs):
    from textwrap import dedent as d, indent as i
    v = vars(args).copy()
    v['output'] = v['output'].name
    if v['output'] == '<stdout>': v['output'] = '-'
    
    o = [f'  --{ol}' if isinstance(ov,bool) else
         f'  --{ol} {ov}' for ol,ov in v.items()
         if ov != defs[ol] and ol != 'input']
    s = '\n    %% '.join(o)
    return d(f'''    %% -*- LaTeX -*-
    %%
    %% This file was created by `wgcsv2chits.py' from the input
    %% `{v['input'].name}' with options
    %% {s}
    %%
    ''')

# --------------------------------------------------------------------
def format_data(db):
    from textwrap import dedent as d, indent as i
    s = ''
    oldc = db[0][0]
    for c,r,t in db:
        if oldc != c:
            s    += '  %\n'
            oldc =  c

        s += f'  {t}'+'\n'
        
    return r'\newcommand\Hexes{'+'\n'+s+'}%\n'

# --------------------------------------------------------------------
def format_package(db,
                   output,
                   custom=''):
    from pathlib import Path
    from textwrap import dedent as d, indent as i
    out = Path(output.name)
    cst = fr'\RequirePackage{{{custom}}}' if custom else ''
    dcl = fr'\ProvidesPackage{{{out.stem}}}'
    dfs = format_data(db)
    dfs = i(dfs, '        ').strip()
    s = d(fr'''
        {dcl}
        \RequirePackage{{wargame}}
        {cst}
        {dfs}
        %% Local Variables:
        %%   mode: LaTeX
        %% End:
        ''')
    return s

# --------------------------------------------------------------------
def format_standalone(db,
                      custom=None,
                      cls='standalone'):
    from textwrap import dedent as d, indent as i
    cst                 = fr'\usepackage{{{custom}}}' if custom else ''
    dcl                 = (r'\documentclass[tikz]{standalone}'
                           if cls == 'standalone' else
                           fr'\documentclass{{cls}}')
    dfs = format_data(db)
    dfs = i(dfs, '        ').strip()
    s = d(fr'''
        {dcl}
        \usepackage{{wargame}}
        {cst}
        {dfs}
        %
        \begin{{document}}
           \begin{{tikzpicture}}
             \Hexes{{}}%
           \end{{tikzpicture}}%
        \end{{document}}
        %% Local Variables:
        %%   mode: LaTeX
        %% End:
        ''')

    return s

# --------------------------------------------------------------------
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType
    from textwrap import fill as f, wrap as w
    
    ap = ArgumentParser(description=('Write LaTeX hex sheet from '
                                     'CSV, Spreadsheet, or JSON file'))
    ap.add_argument('input',type=FileType('r'),help='Input file name')
    ap.add_argument('-o','--output',type=FileType('w'),help='Output file name',
                    default='-')
    ap.add_argument('-p','--package',type=str,help='User package',
                    default=None),
    ap.add_argument('-s','--standalone',action='store_true',
                    help='Create standalone document')
    ap.add_argument('-S','--sheet',type=str,default='0',
                    help='Sheet name or offset')
    ap.add_argument('-H','--full-help',help='Show longer help',
                    action='store_true')
    args  = ap.parse_args()
    defs  = {ol: ap.get_default(ol) for ol in vars(args)}
    if args.full_help:
        ap.print_help()
        print()
        print(__doc__)

        from sys import exit

        exit(0)
        
    hexes = read_specs(args.input,args.sheet)
    db    = format_hexes(hexes)

    s     = format_comment(args,defs)
    if args.standalone:
        if args.output.name.endswith('.sty'):
            s += format_package(db,
                                args.output,
                                custom = args.package)
        else:
            s += format_standalone(db, custom = args.package)
    else:
        s += format_data(db)

    print(s,file=args.output)

# Local Variables:
#  mode: python
# End:


