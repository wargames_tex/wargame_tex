.. LaTeX Wargame exporter documentation master file, created by
   sphinx-quickstart on Tue Nov 22 15:40:06 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LaTeX Wargame exporter's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. automodapi:: wgexport

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
