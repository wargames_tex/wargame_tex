/*
 *
 * Copyright (c) 2022 Christian Holm Christensen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License (LGPL) as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, copies are available
 * at http://www.opensource.org.
 */
package dd;

import java.awt.Point;

import org.apache.commons.lang3.ArrayUtils;

import VASSAL.build.module.map.boardPicker.board.mapgrid.HexGridNumbering;
import VASSAL.build.Buildable;


public class ObliqueHexGridNumbering extends HexGridNumbering {
    public boolean direction = true;
    public static final String DIRECTION = "direction"; //NON-NLS

    @Override
    public void addTo(Buildable parent) { super.addTo(parent); }

    @Override
    public String[] getAttributeDescriptions() {
	return ArrayUtils.add(super.getAttributeDescriptions(),
			      "Slanted right");
    }

    @Override
    public String[] getAttributeNames() {
	return ArrayUtils.add(super.getAttributeNames(), DIRECTION);
    }

    @Override
    public Class<?>[] getAttributeTypes() {
	return ArrayUtils.add(super.getAttributeTypes(), Boolean.class);
    }

    @Override
    public void setAttribute(String key, Object value) {
	if (DIRECTION.equals(key)) {
	    if (value instanceof String) {
		value = Boolean.valueOf((String) value);
	    }
	    direction = (Boolean) value;
	}
	else {
	    super.setAttribute(key, value);
	}
    }

    @Override
    public String getAttributeValueString(String key) {
	if (DIRECTION.equals(key)) {
	    return String.valueOf(direction);
	}
	else {
	    return super.getAttributeValueString(key);
	}
    }

    @Override
    protected String getName(int row, int column)
    {
	// final int    maxRows = getMaxRows();
	// final int    maxCols = getMaxColumns();
	int orow = row - (int)Math.floor(column/2)+1;
	if (direction)
	    orow = (int)Math.floor(column/2) + row;
	
	final String rowName = getName(orow   + vOff, vType, vLeading);
	final String colName = getName(column + hOff, hType, hLeading);
	if (first == 'H') {
	    return colName + sep + rowName;
	}
	return rowName + sep + colName;
	// String s = super.getName(row,column);
    }
    @Override
    public int getRow(Point p) {
	int srow = super.getRow(p);
	return srow;
    }
}
// EOF
