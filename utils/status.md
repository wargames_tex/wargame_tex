# AIR command 

## Main

- air decoy 
  air decoy
- airborne command post 
  text=ACP
- airborne early warning 
  text=AEW
- anti submarine warfare 
  text=ASW
- anti surface warfare 
  squashed text=ASUW
- attack
  text=A
- bomber 
  text=B
- cargo 
  text=C
- civilian airship
  airship 
- civilian balloon
  balloon 
- civilian fixed wing
  fixed wing
- civilian rotary wing
  rotary wing
- civilian 
  text=CIV
- combat search and rescue 
  squashed text=CSAR
- communications 
  text=COM
- electronic support measures 
  text=ESM
- fighter
  text=F
- government
  text=GOV
- jammer
  text=J
- medic
  [fill]medic
- military airship
  [fill]airship
- military balloon
  [fill]balloon
- military fixed wing
  [fill]fixed wing
- military rotary wing
  [fill]rotary wing
- military
  text=MIL
- mine countermeasures
  text=MCM
- passenger
  text=PX
- patrol
  text=P
- personnel recovery
  text=PR
- reconnaissance
  text=R
- search and rescue 
  text=SAR
- special operations forces 
  text=SOF
- suppression of enemy air defence 
  squashed text=SEAD
- tanker 
  text=K
- trainer 
  text=T
- ultra light
  text=UL
- unmanned aerial vehicle
  [fill]unmanned
- utility
  text=U
- vertical short takeoff and landing 
  text=V
- very important person
  text=VIP

## Upper
- airborne command post
  text=ACP
- airborne early warning
  text=AEW
- anti submarine warfare
  text=ASW
- anti surface warfare
  text=ASUW
- cargo
  text=C
- combat search and rescue
  text=CSAR
- communications
  text=COM
- electronic support measures
  text=ESM
- escort
  text=E
- government flight
  text=GOV
- intensive care
  text=IC
- jammer
  text=R
- medical evacuation
  [fill]medic
- mine countermeasures
  text=MCM
- passenger plane
  text=PX
- patrol
  text=J
- photography
  text=PH
- reconnaissance
  text=P
- search and rescue
  text=SAR
- special operations forces
  text=SOF
- suppression of enemy air defence
  squashed text=SEAD
- tanker
  text=K
- trainer
  text=T
- ultra light
  text=UL
- utility
  text=U
- very important person
  text=VIP
  
## Lower
- boom and drogue
  text=B/D
- boom only
  text=B
- close range
  text=CR
- drogue only
  text=D
- heavy
  text=H
- light
  text=L
- long range
  text=LR
- medium range
  text=MR
- medium
  text=M
- short range
  text=SR
  
# MISSILE comand
## Main

- missile
  missile
- air 
  main=missile,left=text=A
- anti ballistic 
  main=missile,left=text=AB
- ballistic 
  main=missile,left=text=B
- cruise 
  main=missile,left=text=C
- space 
  main=missile,left=text=SP
- sub surface 
  main=missile,left=text=SU
- surface
  main=missile,left=text=S

# LAND command
## Main

- above corps support
  above corps support
- administrative
  text=ADM
- air and naval gunfire liaison company
  [fill]artillery,reconnaissance,air traffic,upper=naval
- air assault with organic lift
  air assault with organic lift
- air defence
  air defence
- air traffic services
  air traffic,[fill]individual
- ammunition
  ammunition
- amphibious
  amphibious
- analysis electronic warfare
  analysis,electronic warfare wide
- analysis
  analysis
- anti tank anti armour
  anti tank anti armour
- armoured engineer
  armoured,[scale=.7]engineer
- armoured
  armoured
- aviation composite fixed wing and rotary wing
  [fill]fixed and rotary wing
- aviation fixed wing
  fixed wing
- aviation rotary wing
  rotary wing
- band
  text=BAND
- broadcast transmitter antenna
  antenna
- chemical biological radiological nuclear defence
  chemical biological radiological nuclear
- civil affairs
  text=CA
- civilian military cooperation
  civilian military cooperation
- civilian police
  civilian police
- combat service support
  text=CSS
- combat support
  combat support
- combat
  text=CBT
- combined arms
  combined arms
- corps support
  corps support
- counter intelligence
  text=CI
- criminal investigation division 
  text=CID
- direction finding electronic warfare
  direction finding,electronic warfare wide
- diving
  diving
- dog
  text=DOG
- drilling
  drilling
- electronic ordinance disposal
  text=EOD
- electronic ranging
  electronic ranging
- electronic warfare
  text=EW
  Note: electronic warfare wide 
- engineer
  engineer 
- environmental protection
  environmental protection
- field artillery observer
  {[clip]observer},{[scale=.7,shift={(0,-.1)},fill]artillery},reconnaissance
- field artillery
  [fill]artillery
- finance
  finance
- fire protection
  fire protection
- geospatial support
  text=GEO
- government organization
  text=GO
- headquarters
  headquarters
- individual
  individual
- infantry
  infantry
- information operations
  text=IO
- intercept electronic warfare
  [fill]analysis,electronic warfare wide
- intercept
  [fill]analysis
- internal security force 
  text=ISF
- interrogation 
  text=IPW
- jamming electronic warfare 
  jamming,text=EW
- jamming
  jamming
- joint fire support 
  text=JFS
- judge advocate general
  text=JAG
- killing victim
  killing,individual
- killing victims
  killing,group
- labour
  labour
- laundry
  laundry
- liaison
  text=LO
- main gun system
  main gun
- maintenance
  maintenance=fill
- material
  text=MAT
- medical treatment facility
  medical treatment
- medical
  medical
- meteorological
  text=MET
- military intelligence 
  text=MI
- military police 
  text=MP
- mine
  mine
- missile 
  land missile
- morale welfare and recreation
  text=MWE 
- mortar
  mortar
- mortuary affairs
  mortuary affairs
- motorized
  motorized
- naval
  naval 
- observer
  observer
- ordnance
  ordnance
- organisation or group
  organisation
- personnel services
  text=PS
- petroleum oil and lubricants
  fuel
- pipeline
  pipeline
- postal
  postal
- psychological operations broadcast
  antenna,{[scale=.7,fill]psychological}
- psychological
  psychological
- public affairs
  text=PA
- quartermaster
  quartermaster
- radar
  radar
- radio relay:
  radio relay
- radio teletype centre
  radio teletype
- radio
  radio
- reconnaissance
  reconnaissance
- religious support
  text=REL
- replacement holding unit
  text=RHU
- sea air land 
  squashed text=SEAL
- search electronic warfare
  search,electronic warfare wide
- search
  search
- security
  text=SEC
- self propelled field artillery
  motorized,artillery
- sensor
  sensor
- shore patrol security police
  text=SP
- signal radio relay
  signal,radio relay
- signal radio teletype centre
  signal,radio teletype
- signal radio
  signal,radio
- signal tactical satellite
  signal,tactical satellite
- signal
  signal
- sniper:
  sniper
- special forces
  text=SF
- special operations forces
  text=SOF
- spy:
  text=SPY
- supply
  supply
- surveillance
  [fill]observer
- survey
  survey
- sustainment
  squashed text=SUST
- tactical mortar
  armoured,[scale=.6]mortar
- tactical satellite
  tactical satellite
- topographic
  topographic
- transportation
  transportation
- unmanned systems
  unmanned
- victim of an attempted crime
  individual,crime
- video imagery
  video imagery
- water purification
  water,{[shift={(-.2,.-.1)}]small squashed text=PURE}
- water
  water

## Upper
- Mil medical role 1
  text=1
- Mil medical role 2
  text=2
- Mil medical role 3
  text=3
- Mil medical role 4
  text=4
- air assault
  air assault
- area
  squashed text=AREA
- assassination
  text=AS
- attack
  text=A
- biological
  text=B
- border
  text=BOR
- bridging
  bridge
- chemical
  text=C
- close protection
  text=CLP
- coerced or impressed recruit
  text=C
- combat
  text=CBT
- command and control
  text=C2
- communications contingency package
  text=CCP
- construction
  squashed text=CONST
- cross cultural communication
  text=CCC
- crowd and riot control
  text=CRC
- decontamination
  text=D
- detention
  text=DET
- direct communications
  direct communications
- displaced persons refugees and evacuees
  squashed text=DPRE
- diving
  diving
- division
  text=XX
- dog
  text=DOG
- drilling
  drilling
- electro optical
  text=EO
- enhanced
  text=ENH
- execution
  text=EX
- explosive ordnance disposal
  text=EOD
- fire direction centre
  text=FDC
- force
  text=F
- foreign fighters
  text=FF
- forward
  text=FWD
- gang member or gang
  squashed text=GANG
- government organisation
  text=GO
- ground station module
  text=GSM
- hijacking
  text=H
- kidnapping
  text=K
- landing support
  text=LS
- large extension node
  text=LEN	
- leader or leadership
  text=LDR
- maintenance:
  maintenance
- mine countermeasuure
  text=MCM
- missile:
  land missile
- mobile advisor and support
  mobile advisor and support
- mobile subscriber equipment
  text=MSE
- mobility support
  text=MS
- movement control centre
  text=MCC
- multinational specialized unit
  text=MSU
- multinational
  text=MN
- multiple rocket launcher
  rocket launcher=multiple head
- murder victims
  text=MU
- naval
  naval
- node centre
  text=NC
- non-governmental organization member or non-governmental
  organisation
  text=NGO
- nuclear:
  text=N
- operations:
  text=OPS
- piracy:
  text=PI
- radar:
  radar
- radiological
  text=RAD
- rape
  text=RA
- religious or religious organisation
  text=REL
- runway
  runway
- search and rescue
  text=SAR
- security
  text=SEC
- sensor control module
  text=SEM
- sensor
  sensor
- signals intelligence
  signals intelligence
- single rocket launcher
  rocket launcher=single head
- single shelter switch
  text=SSS
- smoke
  text=S
- sniper
  sniper
- sound ranging
  text=SDR
- special weapons and tactics
  squashed text=SWAT
- survey:
  survey
- tactical exploitation
  text=TE
- target acquisition
  text=TA
- targeted individual or organisation
  text=TGT
- terrorist or terrorist organisation
  text=TER
- topographic
  topographic
- utility
  text=U
- video imagery
  video imagery
- willing recruit
  text=W

## Lower

- airborne
  airborne
- arctic
  arctic
- battle damage repair
  text=BDR
- bicycle equipped
  bicycle equipped
- clearing
  text=CLR
- close range
  text=CR
- control
  control
- decontamination
  text=D
- demolition
  text=DEM
- dental
  text=D
- digital
  text=DIG
- enhanced location reporting system
  enhanced location reporting system
- equipment
  text=E
- heavy
  text=H
- intensive care
  text=IC
- intermodal
  intermodal
- laboratory
  text=LAB
- launcher
  launcher
- light
  text=L
- long range
  text=LR
- medium range
  text=MR
- mountain
  mountain
- multi channel
  text=MC
- optical
  text=OPT
- pack animal
  pack animal
- patient evacuation coordination 
  text=PEC
- preventative maintenance
  text=PM
- psychological
  text=P
- radio relay line of sight
  radio relay line of sight
- railroad
  railroad
- recovery maintenance
  maintenance
- recovery unmanned systems
  recovery unmanned systems
- rescue coordination centre
  text=RCC
- riverine
  riverine
- short range
  text=SR
- single channel
  text=SC
- ski
  ski
- strategic
  text=STR
- support
  text=SPT
- tactical
  text=TAC
- towed
  towed
- troop
  text=T
- vertical of short takeoff and landing
  squashed text=VSTOL
- veterinary
  text=V
- wheeled
  wheeled
  
# EQUIPMENT command

## Main

- air defence gun
  gun=air defence
- air defence missile launcher
  missile launcher=air defence
- antennae
  antenna
- anti tank gun
  gun=anti tank
- anti tank missile launcher
  missile launcher=anti tank
- anti tank rocket launcher
  rocket launcher=anti tank
- antipersonnel land mine
  land mine=personnel
- antitank land mine
  land mine=tank
- armoured fighting vehicle command and control
  armoured fighting vehicle,small text=C2
- armoured fighting vehicle
  armoured fighting vehicle
- armoured medical personnel carrier
  {[save clip]armoured personnel carrier},{[scale=.7]medical}
- armoured personnel carrier
  armoured personnel carrier
- armoured protected recovery vehicle
  armoured,[scale=.7]maintenance
- armoured protected vehicle
  armoured
- automatic rifle
  rifle,type=heavy
- bomb
  squashed text=BOMB
- booby trap
  booby trap
- bridge mounted on utility vehicle
  utility vehicle,[scale=.7]bridge
- bridge
  bridge
- bus
  utility vehicle,text=B
- chemical biological radiological nuclear equipment
  chemical biological radiological nuclear
- computer system
  computer system
- direct fire gun
  gun=direct
- drill mounted on vehicle
  utility vehicle,{[scale=.6,yshift=-3]drilling}
- drill
  drilling
- earthmover
  earthmover
- fixed bridge
  bridge=fixed
- flame thrower
  flame thrower
- folding girder bridge 
  bridge=folding
- generator set
  text=G
- grenade launcher
  grenade launcher
- heavy grenade launcher
  grenade launcher,[yshift=-.2]type=heavy
- heavy machine gun
  machine gun,type=heavy
- heavy tank
  tank,type=vheavy
- hollow deck bridge
  bridge=hollow
- howitzer
  howitzer
- improvised explosive device
  text=IED
- land mine
  land mine
- laser
  laser
- light grenade launcher
  grenade launcher,[yshift=-.2]type=light
- light machine gun
  machine gun,type=light
- light tank
  tank,type=vlight
- machine gun
  machine gun
- medical evacuation armoured protected vehicle
  armoured,[fill]medic
- medical evacuation medical vehicle
  utility vehicle,[fill]medical
- medium grenade launcher
  grenade launcher,[yshift=-.2]type=medium
- medium machine gun
  machine gun,type=medium
- medium tank
  tank,type=vmedium
- mine clearing equipment
  mine clearing equipment
- mine clearing vehicle
  tank,mine clearing equipment
- mine laying equipment
  mine,upper=type=light
- mine laying vehicle
  utility vehicle,mine,upper=type=light
- missile launcher
  missile launcher
- mobile emergency physician
  [save clip]utility vehicle,physician
- mortar
  mortar
- multifunctional earthmover
  earthmover,text=MF
- multiple rocket launcher
  rocket launcher=multiple
- non lethal grenade launcher
  grenade launcher=non lethal
- non lethal weapon
  non lethal weapon
- petroleum oil and lubricants vehicle
  utility vehicle,[scale=.6]fuel
- psychological operations equipment
  psychological
- radar
  radar
- recoilless gun
  gun=recoilless
- rifle
  rifle
- semi automatic rifle
  rifle,type=medium
- semi trailer truck
  semi trailer truck
- sensor emplaced
  sensor,upper=jagged wave
- sensor
  sensor
- single rocket launcher
  rocket launcher=single
- single shot rifle
  rifle,type=light
- surface to surface missile launcher
  missile launcher=surface to surface
- tank recovery vehicle
  tank,[scale=.8]maintenance
- tank
  tank
- taser
  non lethal weapon,text=Z
- train locomotive
  train locomotive
- utility vehicle
  utility vehicle
- water cannon
  non lethal weapon,text=W
- water vehicle
  utility vehicle,{[yshift=-3,scale=.6]water}
  
## Lower

- amphibious
  below=amphibious
- barge
  below=riverine
- over snow
  below=over snow
- pack animal
  below=pack animal
- railroad
  below=railroad
- sled
  below=sled
- towed
  below=towed
- tracked
  below=tracked
- wheeled and tracked
  below=wheeled=and tracked
- wheeled cross country
  below=wheeled=cross country
- wheeled limited mobility
  below=wheeled=limited
- wheeled semi trailer
  below=wheeled=semi
  
# INSTALLATION Command

## Main

- ammunition cache
  ammunition,supply
- black list location
  text=BLK
- broadcast transmitter antenna
  antenna
- chemical biological radiological nuclear
  chemical biological radiological nuclear
- civilian telecommunications
  civilian telecommunications
- electric power
  electric power
- food distribution
  food,supply
- grey list location
  squashed text=GRAY
- mass grave site
  {[scale=.7,yshift=-.05]mortuary affairs},{[scale=.7,shift={(-.22,.05)}]mortuary affairs},{[scale=.7,shift={(.22,.05)}]mortuary affairs}
- medical treatment facility
  medical treatment
- medical
  medical
- mine
  quarry
- naval
  naval
- nuclear
  nuclear
- printed media
  printed media
- safe house
  squashed text=SAFE
- transportation
  transportation
- water treatment
  water,{[shift={(-.2,.-.1)}]small squashed text=PURE}
- water
  water
- white list location
  text=WHT
  
## Upper

- biological
  text=B
- chemical
  text=C
- coal
  text=CO
- geothermal
  text=GT
- hydroelectric
  text=HY
- natural gas
  text=NG
- nuclear energy
  nuclear
- nuclear
  text=N
- petroleum
  fuel
- radio
  text=R
- railroad
  railroad
- telephone
  text=T
- television
  text=TV
- yard
  text=YRD
  
# SEA SURFACE Command

## Main 

- ammunition ship
  text=AE
- amphibious assault ship
  text=LHA
- amphibious assault
  text=LA
- amphibious assualt ship helicopter 
  text=LPH
- amphibious command ship 
  text=LCC
- amphibious transport 
  text=LPD
- amphibious warfare ship
  amphibious warfare ship
- auxiliary flag ship
  text=AGF
- auxiliary ship
  text=AA
- barge
  ship,[shift={(0,-.05)}]small text=YB
- battleship
  text=BB
- cargo
  ship,text=A
- carrier
  carrier
- civilian boat
  boat
- civilian jetski
  jetski
- civilian rigid hull inflatable boat
  boat,[shift={(0,-.05)}]small text=RB
- civilian speedboat
  boat,[shift={(0,-.05)}]small text=SP
- civilian unmanned surface water vehicle
  unmanned
- civilian 
  text=CIV
- combat support ship 
  text=AOE
- combatant
  combatant
- container ship
  ship,text=C
- convoy
  convoy
- corvette 
  text=FS
- cruiser guided missile 
  text=CG
- destroyer 
  text=DD
- dredge 
  ship,text=D
- drifter
  fishing vessel,text=DF
- ferry 
  ship,text=F
- fishing vessel
  fishing vessel
- frigate 
  text=FF
- harbour tug 
  text=YT
- hazardous material transport ship 
  ship,[yshift=.-.07]small text=HZ
- heavy lift 
  ship,text=H
- hospital ship
  text=AH
- hovercraft
  ship,text=J
- intelligence collector
  text=AGI
- junk 
  ship,[yshift=-.07]small text=QJ
- landing craft 
  text=LC
- landing ship 
  text=LS
- lash carrier 
  ship,text=L
- launch
  text=YFT
- law enforcement vessel
  coast guard vessel
- littoral combatant ship
  text=LCS
- military jetski
  [fill]jetski
- military rigid hull inflatable boat
  {[fill]boat},{[white,yshift=-0.7]small text=RB}
- military speedboat
  [fill]boat
- military unmanned surface water vehicle
  [fill]unmanned
- military 
  text=MIL
- mine countermeasure support ship 
  text=MCS
- mine countermeasures 
  text=MCM
- mine warfare vessel
  mine warfare vessel
- minehunter 
  text=MH
- minelayer 
  text=ML
- minesweeper drone
  text=MSD
- minesweeper 
  text=MS
- multi purpose amphibious assualt ship 
  text=LHD
- naval cargo ship 
  text=AK
- navy task element 
  navy task,squashed text=TE
- navy task force 
  navy task,squashed text=TF
- navy task group 
  navy task,squashed text=TG
- navy task organisation unit
  navy task
- navy task unit
  navy task,squashed text=TU
- non combatant
  non combatant
- non self propelled barge
  text=YB
- ocean going tug 
  text=AT
- ocean research ship 
  text=AGO
- oiler 
  text=AOR
- passenger ship
  ship,text=P
- patrol craft 
  text=PC
- patrol ship 
  text=PG
- patrol
  patrol
- repair ship
  text=AR
- roll on roll off
  ship,text=E
- sailing boat
  sailing boat
- sea surface decoy
  decoy
- self propelled barge 
  text=YS
- service craft
  text=YY
- ship
  ship
- stores ship 
  text=AF
- submarine tender 
  text=AS
- surface combatant
  surface combatant
- survey ship
  text=AGS
- tanker
  ship,text=O
- tow 
  ship,[yshift=-.09]small text=TW
- trawler 
  fishing vessel,[yshift=-.09]small text=TR
- tug
  ship,text=T
  
## Upper

- anti air warfare
  text=AAW
- anti submarine warfare
  text=ASW
- ballistic missile
  text=B
- drone equipped
  [fill]unmanned
- electronic warfare
  text=EW
- escort
  text=E
- guided missile
  text=G
- helicopter equipped
  text=H
- intelligence surveillance reconnaissance
  text=ISR
- medical
  text=ME
- mine counter measures
  text=MCM
- mine warfare
  text=MW
- missile defence
  text=MD
- other guided missile
  text=M
- remote multi mission vehicle
  text=RMV
- special operations force
  text=SOF
- surface warfare
  text=SUW
- torpedo
  text=T
  
## Lower

- air cushioned alternate
  text=AC
- air cushioned
  text=J
- autonomous control
  text=AUT
- dock
  text=D
- expendable
  text=EXP
- fast
  text=F
- heavy
  text=H
- hydrofoil
  text=K
- light
  text=L
- logistics
  text=LOG
- medium
  text=M
- nuclear powered
  text=N
- remotely piloted
  text=RP
- tank
  text=T
- vehicle
  text=V
  
# SUB SURFACE command 

## Main

- autonomous underwater vehicle
  [fill]unmanned
- bottomed sea mine decoy
  [fill]sea mine=top half,{[scale=.6,yshift=-6]decoy},lower=bottomed
- bottomed submarine
  submarine,lower=bottomed
- civilian autonomous underwatervehicle
  unmanned
- civilian diver
  diving
- civilian seabed installation
  seabed installation
- civilian submersible
  submersible
- civilian 
  text=CIV
- improvised explosive device 
  text=IED
- military diver
  diving=military
- military seabed installation
  [fill]seabed installation
- military
  text=MIL
- moored sea mine decoy
  [fill]sea mine=top half,{[yshift=-4,scale=.6]decoy},lower=[yshift=-2]bottomed,lower=moored
- non submarine
  small squashed text=NON SUB
- other submersible
  [fill]submersible
- sea mine decoy
  [fill]sea mine=top half,{[scale=.6,yshift=-6]decoy}
- snorkelling submarine 
  submarine,upper=[yshift=-8]surfaced,upper=[yshift=-2]type=vlight
- submarine
  submarine
- surfaced submarine
  submarine,lower=surfaced
- torpedo
  torpedo
- underwater decoy
  decoy,upper=[yshift=-4]bottomed
- underwater weapon
  text=WPN
- unexploded ordnance
  unexploded ordnance 
  
## Upper 

- anti submarine warfare
  text=ASW
- attack
  text=A
- auxiliary
  text=AUX
- ballistic missile
  text=B
- certain submarine
  text=CT
- command and control
  text=C2
- guided missile
  text=G
- intelligence surveillance reconnaissance
  text=ISR
- mine countermeasures
  text=MCM
- mine warfare
  text=MW
- other guided missile
  text=M
- possible submarine high 3
  text=P3
- possible submarine high 4
  text=P4
- possible submarine low 1
  text=P1
- possible submarine low 2
  text=P2
- probable submarine
  text=PB
- special operations force
  text=SOF
- surface warfare
  text=SUW
  
## Lower

- air independent propulsion
  text=AI
- autonomous control
  text=AUT
- diesel propulsion
  text=D
- diesel type 1
  text=D1
- diesel type 2
  text=D2
- diesel type 3
  text=D3
- expendable
  text=EXP
- nuclear propulsion
  text=N
- nuclear type 1
  text=N1
- nuclear type 2
  text=N2
- nuclear type 3
  text=N3
- nuclear type 4
  text=N4
- nuclear type 5
  text=N5
- nuclear type 6
  text=N6
- remotely piloted
  text=RP
  
# SEA MINE Command

## Main 

- free
  sea mine
- free-neutralised
  sea mine=neutralised
- bottomed
  sea mine,lower=bottomed
- bottomed-neutralised
  sea mine=neutralised,lower=bottomed
- moored
  sea mine,lower=moored
- moored-neutralised
  sea mine=neutralized,lower=moored
- floating
  sea mine,lower=floating
- floating-neutralised
  sea mine=neutralised,lower=floating
- in other position
  sea mine,in position
- in other position-neutralised
  sea mine=neutralised,in position
- rising
  sea mine,lower=rising
- rising-neutralised
  sea mine=neutralised,lower=rising
  
# SPACE Command

## Main 

- anti satellite weapon
  [fill]satellite,[scale=.6]rifle
- civilian astronomical satellite
  satellite=astronomical
- civilian bio satellite
  satellite=bio
- civilian capsule
  capsule
- civilian communications satellite
  satellite=communications
- civilian earth observation satellite
  satellite=earth observing
- civilian miniaturised satellite
  satellite=small
- civilian navigational satellite
  satellite=navigation
- civilian orbiter shuttle
  orbiter shuttle
- civilian satellite
  satellite
- civilian space station
  space station
- civilian tether satellite
  satellite=tether
- civilian weather satellite
  [yshift=-4]satellite,text=WX
- military astronomical satellite
  [fill]satellite=astronomical
- military bio satellite
  [fill]satellite=bio
- military capsule
  [fill]capsule
- military communications satellite
  [fill]satellite=communications
- military earth observation satellite
  [fill]satellite=earth observing
- military miniaturised satellite
  [fill]satellite=small
- military navigational satellite
  [fill]satellite=navigation
- military orbiter shuttle
  [fill]orbiter shuttle
- military satellite 
  [fill]satellite=none
- military space station
  [fill]space station
- military tether satellite
  [fill]satellite=tether
- military weather satellite 
  {[fill,yshift=-4]satellite},text=WX
- planet lander 
  text=PL
- reconnaissance satellite
  satellite=reconnaissance
- reentry vehicle 
  text=RV
- satellite 
  text=SAT
- space vehicle
  text=SV
  
  
## Upper 

- geostationary orbit 
  text=GO
- geosynchronous orbit
  text=GSO
- high earth orbit
  text=HEO
- low earth orbit
  text=LEO
- medium earth orbit
  text=MEO
- molinya orbit
  text=MO
  
## Lower 

- infra red
  text=IR
- optical
  text=O
- radar=
  text=R
- signals instelligence
  text=SI
  
# ACTIVITY Command

## Main

- arrest
  arrest
- attempted criminal activity 
  crime,individual
- automobile
  automobile
- demonstration
  squashed text=MASS
- drive by shooting 
  rifle,lower=wheeled
- drug related activities
  squashed text=DRUG
- explosion
  explosion
- extortion 1
  text=\textdollar
- extortion 2 
  text=\pounds
- extortion 3
  text=\texteuro
- extortion 4
  text=\textyen
- fire 
  squashed text=FIRE
- graffiti 
  graffiti
- improvised explosive device explosion 
  explosion,small text=IED
- individual
  individual 
- killing 
  killing,individual 
- patrolling
  patrolling
- pleasure craft 
  sailing boat 
- poisoning 
  poisoning 
- psychological operations 
  psychological
- radio and television psychological operations 
  psychological,signal
- riot
  squashed text=RIOT
- searching
  searching

## Upper 

- assassination
  text=AS 
- execution
  text=EX
- hijack
  text=H
- house to house 
  house
- kidnapping
  text=K
- murder
  text=MU
- piracy
  text=PI
- rape
  text=RA
- written
  text=W
  
