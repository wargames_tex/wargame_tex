#!/usr/bin/env python
# -*- mode: Python -*-
'''Script to generate LaTeX chit code from a CSV, Spreadsheet, or JSON
file

This script takes an input Comma-Separated-Values (CSV) or Spreadsheet
(LibreOffice or Excel), or Java Serialized Object Notation (JSON)
file, and generates LaTeX (using the package `wargame`) code that will
typeset counter sheets.

Each entry in the CSV, Spreadheet, or JSON can have the following
entries (columns in CSV and Spreadsheet):

- `Name`:          The name of the piece
- `Side`:          The side of the piece, sets the style used
- `Full`:          Full frame image to put on piece (background)
- `Faction`:       NATO App6 faction (default friendly)
- `Command`:       NATO App6 command (default land)
- `Type`:          NATO App6 main symbol(s)
- `Echelon`:       NATO App6 echelon
- `Top`:           NATO App6 top symbol
- `Bottom`:        NATO App6 bottom symbol
- `Left`:          NATO App6 left symbol
- `Right`:         NATO App6 right symbol
- `Below`:         NATO App6 equipment mobility symbol
- `Unique`:        Unique identifier 
- `Parent`:        Parent identifier 
- `Range`:         Artillery range
- `CF`:            Combat factor
- `DF`:            Defence factor
- `MF`:            Movement factor
- `Hex`:           Starting hex
- `Turn`:          Starting turn
- `Range-`:        Reduced range
- `CF-`:           Reduced combat factor
- `DF-`:           Reduced defensive factor
- `MF-`:           Reduced movement factor

All columns are optional.

If either of `Range-`, `CF-`, `DF-`, nor `MF-` are present and
non-empty, then the piece is considered double sided.  Otherwise it
will be single sided.

If `DF` is empty or not set, then units will have factors `CF` and
`MF`.  If `Range` is set, then that will be included in the factors too. 

If `Name` is not specified, then it will be deduced from `Side`,
`Unique`, `Parent`, `Type`, and `Echelon`.

Warning: This script is not omnipotent and cannot cover all use cases.

Please also refer to the `wargame` documentation at

    https://gitlab.com/wargames_tex/wargame_tex

for more.

'''

# --------------------------------------------------------------------
def read_specs(file_or_name='units.csv',sheet=0):
    from pathlib import Path
    from sys import stderr
    
    if isinstance(file_or_name,str):
        input = open(file_or_name,'r')
    else:
        input = file_or_name

    name = input.name
    ext  = Path(name).suffix

    meth = {'.csv':  read_csv,         
            '.ods':  read_spreadsheet, 
            '.odf':  read_spreadsheet, 
            '.odt':  read_spreadsheet, 
            '.xls':  read_spreadsheet, 
            '.xlsx': read_spreadsheet, 
            '.xlsm': read_spreadsheet, 
            '.xlsb': read_spreadsheet,
            '.json': read_json,
            }.get(ext,None)

    if meth is None:
        raise RuntimeError(f'Don\'t know how to deal with '
                           f'"{name}"',file=stderr)
    
    l = meth(input,sheet=sheet)

    if isinstance(file_or_name,str):
        input.close()

    return l

    
# --------------------------------------------------------------------
def read_csv(input,**kwargs):
    from csv import DictReader
    
    reader = DictReader(input)
     
    return list(reader)

# --------------------------------------------------------------------
def read_json(input,**kwargs):
    from json import load

    return load(input)

# --------------------------------------------------------------------
def read_spreadsheet(input,sheet=0,**kwargs):
    try:
        from pandas import read_excel

        try:
            sheet = int(sheet)
        except:
            pass
        
        pd = read_excel(input.name,sheet)
        return pd.to_dict('records')
    
    except ImportError as e:
        raise RuntimeError('Pandas not installed') from e
    except:
        raise

# --------------------------------------------------------------------
chit_ind = '            '

# --------------------------------------------------------------------
def format_inner_factors(cf,df,mf,rng):
    if rng == '0':
        rng = ''

    print(cf,df,mf,rng)
    if df != '':
        return f'chit/3 factors={{{cf},{df},{mf}}}'
    if rng != '':
        return f'chit/2 factors artillery={{{cf},{mf},{rng}}}'
    if cf != '' and mf != '':
        if cf.startswith('(') and cf.endswith(')'):
            return f'chit/2 factors defence={{{cf[1:-1]},{mf}}}'
        return f'chit/2 factors={{{cf},{mf}}}'
    if cf != '':
        return f'chit/1 factor={{{cf}}}'
    return f'chit/1 factor={{{mf}}}'

# --------------------------------------------------------------------
def format_factors(cf,df,mf,rng,indent=chit_ind):
    if cf == '' and df == '' and mf == '' and rng == '':
        return ''
    
    inner = format_inner_factors(cf,df,mf,rng)
    if inner == '':
        return ''

    return '\n'+indent+f'factors={{{inner}}},'

# --------------------------------------------------------------------
def format_upper(which,value,indent=chit_ind):
    if which == '' or value == '' or value == '0':
        return ''
    return '\n'+indent+\
        f'upper {which}={{chit/small identifier={{{value}}}}},'
    
# --------------------------------------------------------------------
def format_turn(turn,indent=chit_ind):
    return format_upper('left',turn.strip(),indent)

# --------------------------------------------------------------------
def format_start(start,indent=chit_ind):
    return format_upper('right',start.strip(),indent)

# --------------------------------------------------------------------
def format_full(full,indent=chit_ind):
    if full == '':
        return ''
    return '\n'+indent+f'full={{{full}}},'

# --------------------------------------------------------------------
def format_side(side,indent=chit_ind):
    if side == '':
        return ''
    return '\n'+indent+f'{side},'

# --------------------------------------------------------------------
def format_name(unit):
    from re import sub

    def norm_type(t):
        return sub('(|normal |small )(|squashed )text=','',
                   sub(r'\[[^]]*\]','',t)).replace('/',' ')
    
    side     = get_field(unit, 'Side')
    typ      = get_field(unit, 'Type')
    echelon  = get_field(unit, 'Echelon')
    unique   = get_field(unit, 'Unique')
    parent   = get_field(unit, 'Parent')
    parts    = []
    if side   != '': parts.append(side)
    if unique != '': parts.append(unique.replace('/',' '))
    if parent != '': parts.append(parent.replace('/',' '))
    parts.extend([norm_type(t) for t in typ.split(',')])
    parts.append(echelon)
    
    return ' '.join(parts)

# --------------------------------------------------------------------
def get_field(unit,name):
    from math import isnan 
    field = unit.get(name, '')
    if field == '':
        return field

    if isinstance(field,int) and isnan(field):
        return ''

    if isinstance(field,float):
        if isnan(field):
            return ''
        if round(field) == field:
            return str(int(field))
        
    return str(field).strip()
    
# --------------------------------------------------------------------
def format_symbol(unit,indent=chit_ind):
    from textwrap import dedent as d, indent as i
    
    faction  = get_field(unit, 'Faction')
    command  = get_field(unit, 'Command')
    typ      = get_field(unit, 'Type')
    echelon  = get_field(unit, 'Echelon')
    top      = get_field(unit, 'Top')
    bottom   = get_field(unit, 'Bottom')
    left     = get_field(unit, 'Left')
    right    = get_field(unit, 'Right')
    below    = get_field(unit, 'Below')
    offset   = get_field(unit, 'Offset')
    faction  = 'friendly' if faction == '' else faction
    command  = 'land'     if command == '' else command

    if typ     == '' and \
       echelon == '' and \
       left    == '' and \
       right   == '' and \
       top     == '' and \
       bottom  == '' and \
       below   == '':
        return ''

    if offset != '':
        offset = offset[1:]  if offset[0]  == '(' else offset
        offset = offset[:-1] if offset[-1] == ')' else offset
    lb = '[' if offset != '' else ''
    rb = f']({offset})' if offset != '' else ''
    s = d(f'''
           symbol={{{lb}
             faction={faction},
             command={command},
             echelon={echelon},
             main={{{typ}}},
             bottom={{{bottom}}},
             top={{{top}}},
             left={{{left}}},
             right={{{right}}},
             below={{{below}}}{rb}
           }},''')
    return '\n'+indent+i(s,indent).strip()

# --------------------------------------------------------------------
def format_id(which,name,indent=chit_ind):
    if which == '' or name == '':
        return ''

    return '\n'+indent+'  '+f'{which}={{chit/identifier={{{name}}}}},'
    
# --------------------------------------------------------------------
def format_unit(unit):
    from textwrap import dedent as d
    from pprint import pprint
    
    name     = get_field(unit, 'Name')
    side     = get_field(unit, 'Side')
    unique   = get_field(unit, 'Unique')
    parent   = get_field(unit, 'Parent')
    
    name     = format_name(unit) if name == '' else name
    turn     = format_turn   (get_field(unit, 'Turn'),chit_ind+'  ')
    start    = format_start  (get_field(unit, 'Hex'),chit_ind+'  ')

    print(name)
    if name.strip() == '':
        return None,None,None,None,None

    if side == '':
        side = 'Markers'
    
    rng      = get_field(unit, 'Range')
    cf       = get_field(unit, 'CF')
    df       = get_field(unit, 'DF')
    mf       = get_field(unit, 'MF')
    factors  = format_factors(cf,df,mf,rng,chit_ind+'  ')
    full     = format_full   (get_field(unit, 'Full'),chit_ind)
    sty      = format_side   (side,chit_ind)
    sym      = format_symbol (unit,chit_ind+'  ')
    uni      = format_id     ('unique',unique,chit_ind+'  ')
    par      = format_id     ('parent',parent,chit_ind+'  ')
    rfactors = None
    rrng     = get_field(unit, 'Range-')
    rcf      = get_field(unit, 'CF-')
    rdf      = get_field(unit, 'DF-')
    rmf      = get_field(unit, 'MF-')
    if rcf != '' or rdf != '' or rmf != '' or rrng != '':
        if rrng == '' and rng != '': rrng = rng
        if rcf  == '' and cf  != '': rcf  = cf
        if rdf  == '' and df  != '': rdf  = df
        if rmf  == '' and mf  != '': rmf  = mf
        rfactors = format_factors(rcf,rdf,rmf,rrng,chit_ind+'  /chit/')
        
    s = d(f'''          {name}/.style={{{sty}
            chit={{{full}{sym}{uni}{par}{start}{turn}{factors}
            }}
          }}''').replace('\n','%\n')
    fname = None
    fs    = None
    if rfactors:
        fname = f'{name} flipped'
        sty   = format_side(f'{side} flipped',chit_ind)
        fs = d(f'''          {name} flipped/.style={{
            {name},{sty}{rfactors}
          }}''').replace('\n','%\n')
        
        
    return side,name,fname,s,fs

# --------------------------------------------------------------------
def format_units(units):

    ret = []
    for unit in units:
        fmt = format_unit(unit)
        if not fmt[1]:
            continue
        ret.append(fmt)

    return ret

# --------------------------------------------------------------------
def format_styles(db,ind='  '):
    from textwrap import dedent as d, indent as i
    has_flipped = any([t is not None for _,n,t,_,_ in db])
    s =  r'\tikzset{'+'\n'
    s += i((',\n').join([s for _,_,_,s,_ in db]),'  ')
    if has_flipped:
        s += ',\n'
    s += i((',\n').join([s for _,_,_,_,s in db if s is not None]),'  ')
    s += '\n}%'+'\n'
    return s

# --------------------------------------------------------------------
def format_list(name,names):
    from textwrap import dedent as d, indent as i
    return d(fr'''        %
        \def\{name}Chits{{{{%
          {",%\n          ".join([n for n in names])}}}}}%''')+'\n'

# --------------------------------------------------------------------
def format_lists(db):
    sg =  [n for _,n,t,_,_ in db if t is None]
    ds =  [n for _,n,t,_,_ in db if t is not None]

    s      = ''
    single = len(sg) > 0
    double = len(ds) > 0
    if single:
        s  += format_list('Single',sg)
    if double:
        s  += format_list('Double',ds)

    return s, single, double

# --------------------------------------------------------------------
def format_sheet(name,ncol,space):
    from textwrap import dedent as d, indent as i
    cmd = r'\chits' if name == 'Single' else r'\doublechits'
    lname = name.lower()
    return d(fr'''        %
        \tikzset{{
          {lname} sheet columns/.store in=\{name}SheetColumns,
          {lname} sheet spacing/.store in=\{name}SheetSpacing,
          {lname} sheet columns={ncol},
          {lname} sheet spacing={space}
        }}%
        \newcommand\{name}Sheet[1][]{{%
          \begin{{tikzpicture}}[#1]
             {cmd}{{\{name}Chits}}{{\{name}SheetColumns}}{{\{name}SheetSpacing}}
          \end{{tikzpicture}}}}%
        ''')
    
# --------------------------------------------------------------------
def format_sheets(single,double,ncol=2,space=.02):
    s  = ''
    if single:
        s += format_sheet('Single',ncol,space)
    if double:
        s += format_sheet('Double',ncol,space)
        
    return s

# --------------------------------------------------------------------
def format_sides(db):
    sides = set([s for s,*_ in db])
    sdf   = (r'\tikzset{'+'\n'+
             '  % Customize or override by package\n  '+
             ',\n  '.join([f'{s}/.style={{color=black,fill=white}}'
                           for s in sides])+'\n}\n')
    return sdf

# --------------------------------------------------------------------
def format_comment(args,defs):
    from textwrap import dedent as d, indent as i
    v = vars(args).copy()
    v['output'] = v['output'].name
    if v['output'] == '<stdout>': v['output'] = '-'
    
    o = [f'  --{ol}' if isinstance(ov,bool) else
         f'  --{ol} {ov}' for ol,ov in v.items()
         if ov != defs[ol] and ol != 'input']
    s = '\n    %% '.join(o)
    return d(f'''    %% -*- LaTeX -*-
    %%
    %% This file was created by `wgcsv2chits.py' from the input
    %% `{v['input'].name}' with options
    %% {s}
    %%
    ''')
        
# --------------------------------------------------------------------
def format_data(db,ncol=3,space=.02):
    from textwrap import dedent as d, indent as i
    sty               = format_styles(db)
    lst,single,double = format_lists (db)
    sht               = format_sheets(single,double,ncol,space)
    s                 = d(sty+lst+sht)
    return s,single,double

# --------------------------------------------------------------------
def format_package(db,
                   output,
                   custom='',
                   ncol=3,
                   space=0.02):
    from pathlib import Path
    from textwrap import dedent as d, indent as i
    out                 = Path(output.name)
    cst                 = fr'\RequirePackage{{{custom}}}' if custom else ''
    dcl                 = fr'\ProvidesPackage{{{out.stem}}}'
    dfs, single, double = format_data(db,ncol,space)
    dfs                 = i(dfs, '        ').strip()
    s = d(fr'''
        {dcl}
        \RequirePackage{{wargame}}
        {cst}
        {dfs}
        %% Local Variables:
        %%   mode: LaTeX
        %% End:
        ''')
    return s
# --------------------------------------------------------------------
def format_standalone(db,
                      custom=None,
                      cls='standalone',
                      ncol=3,
                      space=0.02):
    from textwrap import dedent as d, indent as i
    cst                 = fr'\usepackage{{{custom}}}' if custom else ''
    dcl                 = (r'\documentclass[tikz]{standalone}'
                           if cls == 'standalone' else
                           fr'\documentclass{{cls}}')
    dfs, single, double = format_data(db,ncol,space)
    dfs                 = i(dfs, '        ').strip()
    sdf                 = i(format_sides(db), '        ').strip()
    s = d(fr'''
        {dcl}
        \usepackage{{wargame}}
        {sdf}
        {cst}
        {dfs}
        %
        \begin{{document}}
        {r"\SingleSheet" if single else ""}
        {r"\DoubleSheet" if double else ""}
        \end{{document}}
        %% Local Variables:
        %%   mode: LaTeX
        %% End:
        ''')

    return s

# --------------------------------------------------------------------
def remove_output(out):
    from pathlib import Path

    try:
        p = Path(out.name)
        p.unlink(missing_ok=True)
    except:
        pass 

# --------------------------------------------------------------------
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType
    from textwrap import fill as f, wrap as w
    
    ap = ArgumentParser(description=('Write LaTeX counter sheet from '
                                     'CSV, Spreadsheet, or JSON file'))
    ap.add_argument('input',type=FileType('r'),help='Input file name')
    ap.add_argument('-o','--output',type=FileType('w'),help='Output file name',
                    default='-')
    ap.add_argument('-p','--package',type=str,help='User package',
                    default=None),
    ap.add_argument('-s','--standalone',action='store_true',
                    help='Create standalone document')
    ap.add_argument('-n','--ncolumns',type=int, default=3,
                    help='Number of columns in counter sheets')
    ap.add_argument('-w','--spacing',type=float, default=0.02,
                    help='Spacing (in centimetre) between columns')
    ap.add_argument('-S','--sheet',type=str,default='0',
                    help='Sheet name or offset')
    ap.add_argument('-H','--full-help',help='Show longer help',
                    action='store_true')
    args  = ap.parse_args()
    defs  = {ol: ap.get_default(ol) for ol in vars(args)}
    if args.full_help:
        ap.print_help()
        print()
        print(__doc__)

        remove_output(args.output)
        
        from sys import exit

        exit(0)

    try:
        units = read_specs(args.input,args.sheet)
        db    = format_units(units)
    
        s     = format_comment(args,defs)
        if args.standalone:
            if args.output.name.endswith('.sty'):
                t = format_package(db,
                                   args.output,
                                   custom   = args.package,
                                   ncol     = args.ncolumns,
                                   space    = args.spacing)
            else:
                t = format_standalone(db,
                                      custom   = args.package,
                                      ncol     = args.ncolumns,
                                      space    = args.spacing)
        else:
            t,_,_ = format_data(db, 
                                ncol     = args.ncolumns,
                                space    = args.spacing)
    
        s += t
            
        print(s,file=args.output)
    except:
        remove_output(args.output)

        raise
        

# Local Variables:
# mode: Python
# End:
