#!/usr/bin/env python
'''Reads in prototype XML snippets, and possibly images and
puts them in a VMOD - replacing existing prototypes
'''

def read_proto(file,dummy):
    from xml.dom.minidom import parse
    from wgexport import Prototype
    
    dom = parse(file)

    protos = dom.getElementsByTagName(Prototype.TAG)

    ret = []
    for proto in protos:
        p = Prototype(dummy,node=proto)
        ret.append(p)

    return ret


if __name__ == '__main__':
    from argparse import ArgumentParser, FileType
    from pprint import pprint
    from pathlib import Path
    from wgexport import VMod, VerboseGuard, Verbose, BuildFile, ModuleData, DummyElement
    
    ap = ArgumentParser(description='Add/Replace prototypes in VMOD')
    ap.add_argument('vmod',help='Input VMOD',type=str)
    ap.add_argument('prototype',nargs='+',help='Prototype XMLs',
                    type=str)

    args = ap.parse_args()

    dummy      = DummyElement(None,None)
    prototypes = []
    images     = {}
    for proto in args.prototype:
        pinp = Path(proto).with_suffix('.xml')
        if not pinp.is_file():
            print(f'XML file {pinp} does not exists')
            continue

        data = read_proto(pinp.name,dummy)
        prototypes.extend(data)

        for fmt in ['svg','png']:
            iinp = pinp.with_suffix(f'.{fmt}')

            for d in data:
                if iinp.name not in d._node.childNodes[0].nodeValue:
                    continue
                if not iinp.is_file():
                    print('Image file {iinp} does not exists')
                    continue

                with open(iinp,'rb') as img:
                    images[f'images/{iinp.name}'] = img.read()
                break


    with VMod(args.vmod,'r') as vmod:
        buildFile  = BuildFile(vmod.getBuildFile())
        moduleData = ModuleData(vmod.getModuleData())

    
    with VMod(args.vmod,'a') as vmod:
        game = buildFile.getGame()

        prototypesContainer = game.getPrototypes()[0]
        existingPrototypes   = prototypesContainer.getPrototypes(asdict=True)

        for prototype in prototypes:
            traits            = prototype.getTraits()
            existingPrototype = existingPrototypes.get(prototype['name'])

            if existingPrototype:
                print(f'{prototype["name"]} already exists, replacing')
                existingPrototype['description'] = prototype['description']
                existingPrototype.setTraits(*traits)

            else:
                # Replace it
                print(f'{prototype["name"]} doesn\'t exists, adding')
                prototypesContainer.addPrototype(
                    name = prototype['name'],
                    description = prototype['description'],
                    traits=traits)



        replaceFiles = {VMod.BUILD_FILE :
                        buildFile.encode(),
                        VMod.MODULE_DATA :
                        moduleData.encode()}
        replaceFiles.update(images)

        vmod.replaceFiles(**replaceFiles)
                
#
#
#


        
        
        
    
    
