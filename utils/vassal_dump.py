#!/usr/bin/env python

def _read_save(parent):
    from wgexport import SaveIO
    return SaveIO.readSave(parent)
        
def vassal_dump(file,sub):
    from zipfile import ZipFile, ZIP_DEFLATED
    from sys import stderr 

    fname = file.name
    file.close()

    key, lines = None, None
    try:
        key, lines = _read_save(fname)
    except Exception as e:
        pass

    if key is None and lines is None:
        if sub is None or sub == '':
            raise RuntimeError(f'{fname} is probably a module '
                               f'but not internal filename given')
        try:
            print(f'Try to read {fname}/{sub} as a save container')
            with ZipFile(fname,'r') as vmod:
                key, lines = _read_save(vmod.open(sub,'r'))
            
        except Exception as e:
            print(e,file=stderr)
            raise e
    
    for l in lines:
        print(l)
    
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(description='Show content of a VASSAL save or log file')
    ap.add_argument('input',
                    type=FileType('r'),
                    help='Save or log file to show')
    ap.add_argument('--sub-file','-s',type=str,default='',
                    help='Sub vlog/vsav file if input is a module')

    args = ap.parse_args()

    vassal_dump(args.input,args.sub_file)
    
