#!/usr/bin/env python3

import markdown as md
import html.parser
import sys

class Parser(html.parser.HTMLParser):
    NONE  = 0
    START = 1
    SUB   = 2
    LIST  = 3
    ITEM  = 4
    
    def __init__(self,pre=''):
        super(Parser,self).__init__()
        self._data  = list()
        self._state = Parser.NONE
        self._out   = None
        self._pre   = pre

    def __del__(self):
        self.end_file()
        
    def p(self,*args,**kwargs):
        print(*args,**kwargs,file=self._out)
            
    def begin_file(self):
        self.end_file()

        fn = 'cmp_'+self._cmd.replace(' ','')
        self._out = open(fn+'.tex','w')
        self.p(fr'% \iffalse === {self._cmd} === \fi')
                         
    def end_file(self):
        if self._out is None:
            return

        self._out.close()
        self._out = None
    
    def begin_tbl(self):
        self.p(fr'% \iffalse --- {self._what} ---\fi')
        self.p(fr'{self._pre}\CompatTable{{{self._cmd}}}{{{self._what}}}{{{{')

    def end_tbl(self):
        self.p(f'{self._pre}'+f',\n{self._pre}'.join(self._data)+',')
        self.p(f'{self._pre}}}}}'+'\n')
        self._data  = list()
        
    def handle_starttag(self,tag,attrs):
        if tag == 'h1':
            self._state = Parser.START
        elif tag == 'h2':
            self._state = Parser.SUB
        elif tag == 'ul':
            self._state = Parser.LIST
            self.begin_tbl()
        elif tag == 'li':
            self._state = Parser.ITEM

        # print("Got " + tag,file=sys.stderr)

    def handle_endtag(self,tag):
        if tag == 'h1':
            self._state = Parser.NONE
        elif tag == 'h2':
            self._state = Parser.START
        elif tag == 'ul':
            self.end_tbl()
            self._state = Parser.SUB
        elif tag == 'li':
            self._state = Parser.LIST
        else:
            print("End of " + tag,file=sys.stderr)

    def handle_data(self,data):
        if self._state == Parser.START:
            self.section(1,data)
        elif self._state == Parser.SUB:
            self.section(2,data)
        elif self._state == Parser.LIST:
            pass
        elif self._state == Parser.ITEM:
            self.line(data)

        # print('Got data "{}"'.format(data),file=sys.stderr)

        
    def section(self,lvl,data):
        if len(data) <= 0:
            return

        txt = data.split()
        if lvl == 1:
            if len(txt) <= 1:
                # print('Got too little data at level 1: '+data,file=sys.stderr)
                return

            self._cmd  = ' '.join(txt[:-1]).lower()
            self.begin_file()
        else:
            if len(txt) <= 0:
                # print('Got too little data at level 2: '+data,file=sys.stderr)
                return
            
            self._what = txt[0].lower()

    def parse(self,spec,idef,nms,flds):
        for s,f in zip(nms,flds):
            if spec.startswith(s+'='):
                f += [spec.replace(s+'=','')]
                return flds

        flds[idef] += [spec]
        return flds
            
    def line(self,data):
        if len(self._cmd) <= 0:
            print('No command given!',file=sys.stderr)
            return
        if len(self._what) <= 0:
            print('No what given!',file=sys.stderr)
            return
        
        lines = data.split('\n')
        if len(lines) < 2:
            raise ValueError(f'No line to split: {data} -> {lines}')
        mil   = lines[0].strip()
        spec  = lines[1].strip()
        what  = self._what

        if 'TBD'     in spec:
            print(f'TBD: {mil}/{spec}')
            return
        
        main=[]
        left=[]
        right=[]
        top=[]
        bottom=[]
        below=[]
        nms=['main','left','right','upper','lower','below']
        fls=[main,left,right,top,bottom,below]
        for s in spec.split(','):
            self.parse(s,nms.index(what),nms,fls)
            
        sp = []
        for n,s in zip(nms,fls):
            if len(s) <= 0 or len(s[0]) <= 0:
                continue
            sp += [n+'={'+','.join(s)+'}']
        sp = ','.join(sp)

        self._data.append(f'  {mil}/{{{sp}}}')
        
    def run(self):
        with open('status.md') as file:
            super(Parser,self).feed(md.markdown(file.read()))


if __name__ == '__main__':
    from argparse import ArgumentParser
    ap = ArgumentParser(description='Generate compatibility tables')
    ap.add_argument('-c','--comment',action='store_true')
    ap.set_defaults(comment=False)
    args = ap.parse_args()
    
    p = Parser('% ' if args.comment else '')
    p.run()




        

        
