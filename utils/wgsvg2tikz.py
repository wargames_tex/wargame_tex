#!/usr/bin/env python3
#
# Seas: a6adb3
#
# Works best of SVG saved in pixel coordinates (not mm or the like)
#
from sys import stderr, stdout

# --------------------------------------------------------------------
def monkey_patch():
    from svgelements import SVGElement

    def SVGElement__imatmul__(self,other):
        # print('Dummy SVGElement.__imatmul__')
        return self

    SVGElement.__imatmul__ = SVGElement__imatmul__

monkey_patch()

# --------------------------------------------------------------------
class Verb:
    def __init__(self,verbose=False):
        self._verbose = verbose
        self._vindent = ''

    def msg(self,*args,**kwargs):
        if not self._verbose:
            return

        from io import StringIO

        sio = StringIO()
        print(*args,**kwargs,file=sio,end='')
        print(f'{self._vindent}{sio.getvalue()}',file=stderr)

    def msgi(self):
        self._vindent += ' '

    def msgd(self):
        if len(self._vindent) > 0:
            self._vindent = self._vindent[:-1]

verbose = Verb()

def msg(*args,**kwargs):
    verbose.msg(*args,**kwargs)
def msgi():
    verbose.msgi()
def msgd():
    verbose.msgd()
    
# --------------------------------------------------------------------
def open_svg(input,ppi=96):
    from svgelements import SVG

    msg(f'Opening {input.name}')
    svg =  SVG.parse(input,reify=False,ppi=ppi)
    return svg
    
# --------------------------------------------------------------------
def get_bb(svg):
    llx,lly,urx,ury =  svg.bbox()
    msg(f'Bounding box: ll=({llx},{lly}) ur=({urx},{ury})')
    return llx,lly,urx,ury

# --------------------------------------------------------------------
def flush_left(svg):
    from svgelements import Matrix

    msg('Flush left')
    llx,lly,urx,ury = get_bb(svg)
    svg             *= Matrix.translate(-llx,-lly)
    return svg

# --------------------------------------------------------------------
def scale_unit(svg):
    from svgelements import Matrix

    llx,lly,urx,ury =  get_bb(svg)
    scale           =  1 / max(urx,ury)

    msg(f'Scale to unit size: {scale}')
    svg             *= Matrix.scale(scale)

    return svg, scale

# --------------------------------------------------------------------
def scale_to_cm(svg,px_per_cm = 37.79531157367762334497):
    from svgelements import Matrix

    scale           =  1 / px_per_cm

    msg(f'Scale to centimeters: {scale}')
    svg             *= Matrix.scale(scale)

    return svg, scale

# --------------------------------------------------------------------
def scale_to_scale(svg,scale):
    from svgelements import Matrix

    msg(f'Scale by: {scale}')
    svg             *= Matrix.scale(scale)

    return svg, scale

# --------------------------------------------------------------------
def shift_to_centre(svg):
    from svgelements import Matrix

    msg('Centering')
    llx,lly,urx,ury = get_bb(svg)
    svg             *= Matrix.translate(-urx/2,-ury/2)
    svg             *= Matrix.scale(1,-1)
    svg.reify()
    svg.render()
    return svg

# --------------------------------------------------------------------
def flipy(svg):
    from svgelements import Matrix

    msg('Flip vertical coordinates')
    llx,lly,urx,ury = get_bb(svg)
    svg             *= Matrix.translate(-llx,-lly)
    svg             *= Matrix.scale(1,-1)
    svg.reify()
    svg.render()
    llx,lly,urx,ury = get_bb(svg)
    svg             *= Matrix.translate(0,-lly)
    return svg

# ====================================================================
class Txt:
    def __init__(self,e):
        e.render()
        e.reify()
        llx,lly,urx,ury = e.bbox()

        if e.anchor == 'start':
            x = llx
            y = lly # (lly+ury)/2 # lly
        elif e.anchor == 'end':
            x = urx
            y = ury # (lly+ury)/2 # ury
        else:
            x = (llx+urx)/2
            y = (lly+ury)/2

        self._x       = x
        self._y       = y
        self._text    = e.text
                
    def tikz(self,label,format,sty,as_pic=False,indent=''):
        a = ((',\n'+indent+sty.args(indent,True,format))
             if not sty is None else '')
        s = sty.size() if not sty is None else ''
        s = f'{s} ' if len(s) > 0 else ''
        c = fr'\node[transform shape,inner sep=0pt{a}] ({label})'
        if not as_pic:
            c += f' at ({self._x:{format}},{self._y:{format}})'
        c += f' {{{s}{self._text}}};%'
        # msg(f'Tikz text: {self._text}')
        return c

# --------------------------------------------------------------------
class Sty:
    classes  = {}
    families = {}
    scale    = 1
    ppi      = 96
    for_pic  = False

    @classmethod
    def px2pt(cls,px):
        return px * 72.27 / cls.ppi * cls.scale
    
    @classmethod
    def rgb(cls,col):
        if col is None or col.value is None:
            return None
        # print(f'alpha={col.alpha} ({col.value:x})')
        r = col.red
        g = col.green
        b = col.blue

        return f'{{rgb,255:red,{r};green,{g};blue,{b}}}'

    @classmethod
    def srgb(cls,col):
        if col is None:
            return None
        
        from svgelements import Color
        return cls.rgb(Color(col))
    
    @classmethod
    def wth(cls,width):
        if width is None:
            return None

        try:
            return Sty.px2pt(float(width))
        except:
            pass 

        return None 

    @classmethod
    def fam(cls,family):
        if family is None:
            return None
        family = family.strip("'").strip('"')
        return cls.families.get(family,family)

    @classmethod
    def ser(cls,weight):
        if weight is None:
            return None
        try:
            return 'b' if int(weight) > 600 else 'm'
        except:
            pass

        return ('b' if weight == 'bold' else
                'l' if weight == 'lighter' else 'm')

    @classmethod
    def shp(cls,style):
        if style is None:
            return None
        
        return ('it' if style == 'italic' else
                'sl' if style == 'oblique' else 'n')

    @classmethod
    def siz(cls,size,e=None):
        if size is None:
            return None

        # if isinstance(size,str) and '%' in size:
        #     msg(f'Relative font size: "{size}"')
        #     rel = float(size) / 100

            
        try:
            msg(type(size),size)
            ret = Sty.px2pt(float(size)) * .45
            msg(f'{float(size)}px -> {ret}pt')
        except:
            pass

        return None

    @classmethod
    def opa(cls,opacity):
        if opacity is None:
            return None

        try:
            return float(opacity)
        except:
            pass
        
        return None


    @classmethod
    def fs(cls,sty):

        f = []
        if not sty._family is None:
            f.append(fr'\fontfamily{{{sty._family}}}')
        if not sty._series is None:
            f.append(fr'\fontseries{{{sty._series}}}')
        if not sty._shape is None:
            f.append(fr'\fontshape{{{sty._shape}}}')
        if not sty._size is None:
            f.append(fr'\fontsize{{{sty._size:f}}}{{0}}')

        a = []
        if len(f):
            f.append(r'\selectfont')
            a.append(f'font={"".join(f)}')
        if not sty._anchor is None:
            a.append(f'anchor={sty._anchor}')
        
        return a

    @classmethod
    def col(cls,sty,text=False,format='f'):
        args = []
        if not sty._fill is None:
            if text:
                args.append(f'text={sty._fill}')
            else:
                args.append(f'fill={sty._fill}')
        if not sty._draw is None and not text:
            args.append(f'draw={sty._draw}')
        if not sty._width is None and sty._width > 0:
            args.append(f'line width={sty._width*Sty.scale:{format}}pt')
        if not sty._opacity is None and sty._opacity < 1:
            args.append(f'opacity={sty._opacity}')

        return args

    @classmethod
    def anc(cls,anchor):
        if anchor is None:
            return None

        if anchor == 'start': return 'south west'
        if anchor == 'end'  : return 'north east'
        return 'center'

    @classmethod
    def rot(cls,rotation):
        from svgelements import Angle
        
        if rotation is None:
            return None

        if isinstance(rotation,str):
            rotation = Angle(rotation)

        return rotation.as_degrees

    @classmethod
    def get_attr(cls,e):
        return {
            'fill':     Sty.rgb(e.fill          if hasattr(e,'fill')   else None),
            'draw':     Sty.rgb(e.stroke        if hasattr(e,'stroke') else None),
            'width':    Sty.wth(e.stroke_width  if hasattr(e,'stroke_width') else None),
            'anchor':   Sty.anc(e.anchor        if hasattr(e,'anchor')      else None),
            'family':   Sty.fam(e.font_family   if hasattr(e,'font_family') else None),
            'shape':    Sty.shp(e.font_style    if hasattr(e,'font_style')  else None),
            'series':   Sty.ser(e.font_weight   if hasattr(e,'font_weight') else None),
            'size':     Sty.siz(e.font_size     if hasattr(e,'font_size')   else None),
            'rotate':   Sty.rot(e.rotation      if hasattr(e,'rotation')    else None),
            'opacity':  Sty.opa(e.values.get('opacity',None)),
            'class':    e.values.get('class',None)
            #'stretch':  e.font_stretch
            #'variant':  e.font_variant
        }

    @classmethod
    def get_values(cls,e):
        return {
            'fill':     Sty.srgb(e.values.get('fill',       None)),
            'draw':     Sty.srgb(e.values.get('stroke',     None)),
            'width':    Sty.wth(e.values.get('stroke_width',None)),
            'anchor':   Sty.anc(e.values.get('anchor',      None)),
            'family':   Sty.fam(e.values.get('font_family', None)),
            'shape':    Sty.shp(e.values.get('font_style',  None)),
            'series':   Sty.ser(e.values.get('font_weight', None)),
            'size':     Sty.siz(e.values.get('font_size',   None)),
            'opacity':  Sty.opa(e.values.get('opacity',     None)),
            'rotate':   Sty.rot(e.values.get('rotation',    None)),
            'class':    e.values.get('class',None)
            #'stretch':  e.font_stretch,
            #'variant':  e.font_variant,
        }
        
    
    def __init__(self,e):
        # try:
        #     e.reify()
        # except:
        #     pass

        val           = Sty.get_values(e)
        self._fill    = val.get('fill',   None) 
        self._draw    = val.get('draw',   None) 
        self._opacity = val.get('opacity',None) 
        self._width   = val.get('width',  None) 
        self._anchor  = val.get('anchor', None) 
        self._family  = val.get('family', None) 
        self._shape   = val.get('shape',  None) 
        self._series  = val.get('series', None) 
        self._size    = val.get('size',   None) 
        self._rotate  = val.get('rotate', None) 
        self._class   = val.get('class',  None)

        attr          = Sty.get_attr(e)
        self._fill    = attr.get('fill',   self._fill) 
        self._draw    = attr.get('draw',   self._draw) 
        self._opacity = attr.get('opacity',self._opacity) 
        self._width   = attr.get('width',  self._width) 
        self._anchor  = attr.get('anchor', self._anchor) 
        self._family  = attr.get('family', self._family) 
        self._shape   = attr.get('shape',  self._shape) 
        self._series  = attr.get('series', self._series) 
        self._size    = attr.get('size',   self._size) 
        self._rotate  = attr.get('rotate', self._rotate) 
        self._class   = attr.get('class',  self._class)

        # msg(f'Explicit values',
        #     self._fill,
        #     self._draw,
        #     self._width,
        #     self._anchor,
        #     self._family,
        #     self._shape,
        #     self._series,
        #     self._size,
        #     self._rotate,
        #     self._class)
        
        if self._class is None:
            return

        cls = Sty.classes.get(self._class,None)
        if cls is None:
            from pprint import pprint
            
            cls = Cls(e)
            Sty.classes[self._class] = cls
            fs = e.font_size if hasattr(e,'font_size') else '0'
            msg(f'New class: {self._class}:',fs)
            # pprint(e.values,stream=stderr)

        self.clean_class(cls)

    def clean_class(self,cls):
        if self._fill    == cls._fill:    self._fill    = None
        if self._draw    == cls._draw:    self._draw    = None
        if self._width   == cls._width:   self._width   = None
        if self._anchor  == cls._anchor:  self._anchor  = None
        if self._family  == cls._family:  self._family  = None
        if self._shape   == cls._shape:   self._shape   = None
        if self._series  == cls._series:  self._series  = None
        if self._size    == cls._size:    self._size    = None
        if self._opacity == cls._opacity: self._opacity = None
        

    def is_text(self):
        return (not self._size is None) and self._size > 0

    def size(self):
        if self._size is None or \
           not (self._family is None and \
                self._series is None and \
                self._shape  is None):
            return ''
        
        return fr'\fontsize{{{self._size}}}{{0}}\selectfont'
    
    def args(self,indent='',text=False,format='f'):
        args = []
        # text = self.is_text()
        if not self._class is None:
            args.append(f'  {self._class}')
            #args.extend(self._class.split(' '))
        col  = Sty.col(self,text,format)
        fs   = (Sty.fs(self) if
                not self._family is None and
                not self._series is None and
                not self._shape  is None and
                not self._size   is None else [])
        if len(col) > 0: args.extend(col)
        if len(fs)  > 0: args.extend(fs)
        if not self._rotate is None and self._rotate != 0:
            args.append(f'rotate={self._rotate}')

        return (',\n  '+indent).join(args)
        
        
        
# --------------------------------------------------------------------
class Cls:
    def __init__(self,e):
        attr          = Sty.get_attr(e)
        self._fill    = attr.get('fill',   None)
        self._draw    = attr.get('draw',   None)
        self._opacity = attr.get('opacity',None) 
        self._width   = attr.get('width',  None)
        self._anchor  = attr.get('anchor', None)
        self._family  = attr.get('family', None)
        self._shape   = attr.get('shape',  None)
        self._series  = attr.get('series', None)
        self._size    = attr.get('size',   None)
        
        # msg(f'Derived values',
        #     self._fill,
        #     self._draw,
        #     self._width,
        #     self._anchor,
        #     self._family,
        #     self._shape,
        #     self._series,
        #     self._size)
        
    def is_text(self):
        return (not self._size is None) and self._size > 0
    
    def args(self,indent='',text=False,format='f'):
        text = self.is_text()
        args = []
        col  = Sty.col(self,text,format)
        fs   = Sty.fs(self)
        if len(col) > 0: args.extend(col)
        if len(fs)  > 0: args.extend(fs)
        return (',\n  '+indent).join(args)

    def __str__(self):
        return self.args()
        
# --------------------------------------------------------------------
class Col:
    def __init__(self,fill,stroke,width,opacity,cls):
        self._fill    = Col.rgb(fill)
        self._draw    = Col.rgb(stroke)
        self._opacity = float(opacity) * (1 if fill is None else fill.opacity)
        self._width   = width * 0.74999943307122
        self._class   = cls
        if self._width == 0:
            self._draw = 'none'
        # print(self._opacity)
            
    @classmethod
    def rgb(cls,col):
        if col is None or col.value is None:
            return 'none'
        # print(f'alpha={col.alpha} ({col.value:x})')
        r = col.red
        g = col.green
        b = col.blue

        return f'{{rgb,255:red,{r};green,{g};blue,{b}}}'

    def args(self,format='f'):
        c = ''
        if not self._class is None:
            c += f'{self._class},'
        c += f'fill={self._fill},draw={self._draw}'
        if self._width > 0:
            c += f',line width={self._width:{format}}pt'
        if self._opacity < 1:
            c += f',opacity={self._opacity}'

        return c
    
        
# ====================================================================
def get_paths(svg,namespace=''):
    '''Read in paths from the SVG image 
    
    This will recurse down groups for path operations 

    Returns
    -------
    paths : list 
        List of svg.path.Path objects 
    '''
    from svgelements import Path, Group, Circle, Text, Rect, SVGElement, Title
    from svg.path import parse_path
    from uuid import uuid4
    from pprint import pprint

    msg(f'Get paths in "{namespace}"')
    msgi()
    isns='{http://www.inkscape.org/namespaces/inkscape}'
    paths = []
    
    for e in svg:
        label = namespace + e.values.get(f'{isns}label',
                                         e.values.get('id',
                                                      uuid4().hex[:8]))
        #opac  = e.values.get('opacity',1)
        msg(label)
        sty   = Sty(e)
        if isinstance(e,Group):
            # msg(f'Parse group {label}')
            paths.append((label,get_paths(e,label+'/'),sty))
            continue

        # msg(type(e))
        # if type(e) in [SVGElement,Title]:
        #     continue
        
        if isinstance(e,Path):
            # msg(f'Parse path {label}')
            paths.append((label,parse_path(e.d()),sty))
        elif isinstance(e,Circle):
            # msg(f'Parse circle {label}')
            paths.append((label,parse_path(e.d()),sty))
        elif isinstance(e,Rect):
            # msg(f'Parse rect {label}')
            paths.append((label,parse_path(e.d()),sty))
        elif isinstance(e,Text):
            # msg(f'Parse text {label}')
            if not e.text is None:
                # pprint(e.values)
                paths.append((label,Txt(e),sty))
        else:
            msg(f'Found a {type(e)} element {label}')

    msgd()
    return paths

# ====================================================================
def tikz_point(e,f):
    '''Ouput a point 
    
    Parameters
    ---------- 
    e : complex
        The point 
    f : str 
        Floating point format 
    
    Return
    ------ 
    coord : str 
        The point formatted for Tikz 
    '''
    return f'({e.real:{f}},{e.imag:{f}})'
    
# --------------------------------------------------------------------
def tikz_move(e,f):
    '''Output a move-to operation 
    
    Parameters
    ---------- 
    e : complex
        The point to move to 
    f : str 
        Floating point format 
    
    Return
    ------ 
    coord : str 
        The point formatted for Tikz 
    '''
    return tikz_point(e.start,f)

# --------------------------------------------------------------------
def tikz_line(e,f):
    '''Output a line-to operation 
    
    Parameters
    ---------- 
    e : complex
        The point to draw line to
    f : str 
        Floating point format 
    
    Return
    ------ 
    line : str 
        The line-to operation to point  
    '''
    return '-- '+tikz_point(e.end,f)

# --------------------------------------------------------------------
def tikz_curve(e,f):
    '''Output a curve-to operation 
    
    Parameters
    ---------- 
    e : curve operation (3 complex numbers)
        The control points (2 of them) and the point to draw curve to
    f : str 
        Floating point format 
    
    Return
    ------ 
    curve : str 
        The curve-to operation to point  
    '''
    return \
        '.. controls '+tikz_point(e.control1,f) + \
        ' and '       +tikz_point(e.control2,f) + \
        ' ..'         +tikz_point(e.end,f)

# --------------------------------------------------------------------
def tikz_close(e,f):
    '''Ouput a close (or cycle) operation 
    '''
    return '-- cycle'

# --------------------------------------------------------------------
def tikz_arc(arc,f='8.5f'):
    '''Convert a svt.path.Arc to a TikZ path 

    Parameters
    ----------
    arc : svg.path.Arc 
        Path to convert 
    f : str 
        floating point format to use 
    
    Returns 
    ------- 
    code : list 
        List of Tikz path operations as strings
    '''
    from math import isclose, sin, cos, radians
    s = arc.start
    e = arc.end
    r = arc.radius
    t = arc.rotation
    
    # print(s,e,r,t)
    isfull = isclose(s.real,s.real,rel_tol=.05) and \
        isclose(e.imag,e.imag,rel_tol=.05)

    # print(isfull,arc.start,arc.end)

    if isfull:
        # print(f'ellipse {r} {t}')
        c = complex(-r.real * cos(radians(t)), -r.imag * sin(radians(t)))
        return f'+({c.real:{f}},{c.imag:{f}}) ellipse [x radius={r.real:{f}},y radius={r.imag:{f}},rotate={t:{f}}]'

    # print(f'arc {e} {t} {r}')
    return f'arc ({e.real:{f}},{e.imag})'

# ====================================================================
def path_to_tikz(path,f='8.5f'):
    '''Convert a svt.path.Path to a TikZ path 

    Parameters
    ----------
    path : svg.path.Path 
        Path to convert 
    f : str 
        floating point format to use 
    
    Returns 
    ------- 
    code : list 
        List of Tikz path operations as strings
    '''
    from svg.path import Line, Move, Close, CubicBezier, Path, Arc

    # print(path)
    code = [f'% ']
    for e in path:
        if   isinstance(e, Move):        code.append(tikz_move(e, f))
        elif isinstance(e, Arc):         code.append(tikz_arc(e,f))
        elif isinstance(e, Line):        code.append(tikz_line(e, f))
        elif isinstance(e, Close):       code.append(tikz_close(e,f))
        elif isinstance(e, CubicBezier): code.append(tikz_curve(e,f))
        elif isinstance(e, Path):        code.extend(path_to_tikz(e,f))
        else:
            print(f'Unknown type: {type(e)}',file=stderr)

    return code

# ====================================================================
def doc_header(output=stdout):
    '''Prinout put standalone document header 
    '''
    msg('Making document header')
    print(r'''%
\documentclass{standalone}
\usepackage{tikz}
\usepackage{iftex}
\ifluatex
\else
  \ifxetex
    \usepackage{fontspec}
  \else
  \fi
\fi
\begin{document}%''',file=output)

# --------------------------------------------------------------------
def doc_footer(output=stdout):
    '''Prinout put standalone document footer
    '''
    msg('Finishing document')
    print(r'''%
\end{document}''',file=output)

# --------------------------------------------------------------------
def path_to_pic(path,name,output,format):
    '''Output a Tikz pic definition from the path

    Parameters
    ----------
    path : Path 
        The path to convert
    name : str 
        The name of the pic 
    output : File 
        Output file 
    format : str 
        Floating point format 
    '''
    msg(f'Writing picture {name}')
    
    print(fr'''\tikzset{{% {len(path)}:
  {name}/.pic={{%
    \path[pic actions]''',file=output,end='\n    ')

    if isinstance(path,Txt):
        code = path.tikz(name,format,None,as_pic=True)
    else:
        code = path_to_tikz(path,format)
    print('\n    '.join(code),file=output)
    print('    ;\n  }',file=output)
    print('}%',file=output)

# --------------------------------------------------------------------
def path_to_draw(path,name,sty,output,format,indent='  ',for_pic=False):
    msg(f'Drawing {name}')
    print(f'{indent}%% {name}',file=output)
    if isinstance(path,Txt):
        code = path.tikz(name,format,sty,indent=indent)
        print(f'{indent}{code}',file=output)
        return

    args = 'pic actions'
    cmd  = r'\path'
    if not for_pic:
        args = sty.args(indent,format=format) if not sty is None else ''
        args = ('\n  '+indent+args) if len(args) > 0 else args
        cmd  = r'\draw'
    code = ('\n'+indent).join(path_to_tikz(path,format))
    print(fr'''{indent}{cmd}[{args}]
{indent}{code}
{indent};%''',file=output)
    
# ====================================================================
def write_pics(paths,output,format='f'):
    msg('Writing pictures')
    msgi()
    names = []
    indent = '      '
    explode = False

    top_name = paths[0][0]
    
    o =  fr'\tikzset{{% {len(paths)}:'+'\n'
    o += fr'  {top_name}/.pic={{%'+'\n'
    print(o,file=output)

    class PicSty:
        def args(self,*args,**kwargs): return 'pic actions'

    psty = PicSty()
    
    for name,path,sty in paths:
        if isinstance(path,list):
            msg(f'Making scope {name}')
                
            # args = (sty.args(indent,format=format)
            #         if not sty is None else '')
            # args = ('\n  '+indent+args) if len(args) > 0 else args
            args = ''
            print(fr'{indent}\begin{{scope}}[{args}]%% {name}',
                  file=output)
            write_paths(path,None,output,format,explode,indent+'  ',
                        for_pic=True)
            print(fr'{indent}\end{{scope}}%% {name}',file=output)
        else:
            path_to_draw(path,name,psty,output,format,indent,for_pic=True)

            
    # print(len(paths))
    # for name,path,sty in paths:
    #     print(name,len(path),type(path))
    #     n, p, s = path[0]
    #     print(type(p))
    #     print('p=',p)
    #     path_to_pic(p,name,output,format)
    #     names.append((name,s))

    o =   fr'  }}'+'\n'
    o +=  fr'}}%'+'\n'
    print(o, file=output)
    
    msgd()
    return names

# --------------------------------------------------------------------
def write_paths(paths,names,output,format,explode,indent='    ',
                for_pic=False):
    msg(f'Writing paths {for_pic}')
    msgi()
    if names is not None:
        msg(f'Using pictures')
        x = 0
        y = 0
        for i, (name,sty) in enumerate(names):
            if x > 10:
                x = 0
                y += 2

            at=''
            if explode:
                at = f' at ({x},{y})'

            msg(f'Draw picture {name}')
            args = sty.args('    ',format=format) if not sty is None else ''
            args = '\n    '+args if len(args) > 0 else args
            print(fr'  \pic[{args},pic style]{at} {{{name}}};%',
                  file=output)
            x += 2
    else:
        msg(f'Using direct draw (pic: {for_pic})')
        
        for name,path,sty in paths:
            if isinstance(path,list):
                msg(f'Making scope {name}')

                args = ''
                if not for_pic:
                    args = (sty.args(indent,format=format)
                            if not sty is None else '')
                    args = ('\n  '+indent+args) if len(args) > 0 else args
                print(fr'{indent}\begin{{scope}}[{args}]%% {name}',
                      file=output)
                write_paths(path,None,output,format,explode,indent+'  ',
                            for_pic=for_pic)
                print(fr'{indent}\end{{scope}}%% {name}',file=output)
            else:
                path_to_draw(path,name,sty,output,format,indent,for_pic=for_pic)

    msgd()
                
# ====================================================================
def get_classes(ind='',format='f'):

    clss = []
    nl   = '\n      '
    for nam,cls in Sty.classes.items():        
        clss.append(f'{nam}/.style={{{nl}{cls.args("        ",
                                          format=format)}}}')
        
    # for name,path,color in paths:        
    #     if not color._class is None:
    #         msg(ind,f'Class {color._class} from {name}')
    #         clss.append(f'{color._class}/.style = {{}}')
    #         
    #     if isinstance(path,list):
    #         msg(ind,f'Finding classes in {name}')
    #         clss.extend(get_classes(path,ind+'  '))

    return clss

# ====================================================================
def doit(input='soldier_side.svg',
         centre     = True,
         unit       = True,
         output     = stdout,
         standalone = False,
         format     = '8.5f',
         explode    = True,
         scale      = None,
         ppi        = 96):
    msg('\n '.join([f'Options:',
                    f'centre     = {centre} ',
                    f'unit       = {unit} ',
                    f'standalone = {standalone} ',
                    f'format     = {format} ',
                    f'explode    = {explode}',
                    f'scale      = {scale}',
                    f'ppi        = {ppi}']))

    Sty.ppi    = ppi
    svg        = open_svg(input,ppi)
    orig_bb    = get_bb(svg)

    if centre or unit:
        svg = flush_left(svg)

    uscale = 1
    if unit:
        svg, uscale = scale_unit(svg)
    else:
        svg, _ = scale_to_cm(svg)
    if not scale is None:
        svg, scale = scale_to_scale(svg,scale)

    Sty.scale = uscale * (1 if scale is None else scale)
        
    if centre:
        svg = shift_to_centre(svg)
    else:
        svg = flipy(svg)

    new_bb = get_bb(svg)
    paths  = get_paths(svg)

    if standalone:
        doc_header(output)
    print(f'% Original bounding box: ({orig_bb[0]},{orig_bb[1]}) ({orig_bb[2]},{orig_bb[3]})',
          file=output)
    print(f'% New bounding box:      ({new_bb[0]},{new_bb[1]}) ({new_bb[2]},{new_bb[3]})',
          file=output)

    names = None
    if not standalone:
        names = write_pics(paths,output,format)
    else:
        clss = get_classes(format=format)
        # msg(clss)
        clss = ',\n    '.join(set(clss))
        nl = '\n    '
        print(r'\begin{tikzpicture}[%',file=output)
        print(fr'  pic style/.style={{draw}},{nl}{clss}]',file=output)
        write_paths(paths,names,output,format,explode)
        print(r'\end{tikzpicture}%',file=output)
        
        doc_footer(output)
    
# ====================================================================
def test(svgfile='soldier_side.svg',tikzfile=stdout,standalone=True):
    with open(svgfile,'r') as input:
        with open(tikzfile,'w') if isinstance(tikzfile,str) else tikzfile as output:
            doit(input=input,output=output,standalone=standalone)
    

# ====================================================================
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(description='Convert SVG to unit Tikz picture')
    ap.add_argument('input',type=FileType('r'),nargs='?',default='soldier_side.svg',
                    help='Input SVG file')
    ap.add_argument('-o','--output',type=FileType('w'),default=None,
                    help='Output Tikz file')
    ap.add_argument('-s','--standalone',action='store_true',default=True,
                    help='Write standalone document')
    ap.add_argument('-p','--picture',action='store_false',dest='standalone',
                    help='Only write picture')
    ap.add_argument('-f','--format',type=str,default='8.5f',
                    help='Format for coordinates')
    ap.add_argument('-O','--original-size',dest='unit',action='store_false',
                    help='Do no scale paths to unit size')
    ap.add_argument('-S','--unit-size',dest='unit',action='store_true',
                    help='Scale paths to unit size')
    ap.add_argument('-Z','--no-centre',dest='centre',action='store_false',
                    help='Do not centre paths')
    ap.add_argument('-C','--centre',dest='centre',action='store_true',
                    help='Centre paths')
    ap.add_argument('-X','--explode',dest='explode',action='store_true',
                    help='Explode paths')
    ap.add_argument('-K','--keep',dest='explode',action='store_false',
                    help='Do not explode paths')
    ap.add_argument('-F','--font',action='append',help='Font family substitution')
    ap.add_argument('-l','--scale',type=float,help='Scale factor')
    ap.add_argument('-r','--pixel-per-inch',type=float, default=96,
                    help='Image "Pixels" per inch')
    ap.add_argument('-v','--verbose',action='store_true',help='Be verbose')
    
    args = ap.parse_args()

    verbose._verbose = args.verbose
    fam = [] if args.font is None else \
        [f.split('=') for f in args.font if '=' in f] 
    Sty.families = {k: v for k,v in fam}

    if args.output is None:
        from pathlib import Path
        inname = Path(args.input.name)
        outname = inname.with_suffix('.tex')
        args.output  = open(outname,'w')
        

    doit(input      = args.input,
         unit       = args.unit,
         centre     = args.centre,
         output     = args.output,
         standalone = args.standalone,
         format     = args.format,
         explode    = args.explode,
         scale      = args.scale,
         ppi        = args.pixel_per_inch)
    
#
# EOF
#
