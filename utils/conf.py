# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))
import wgexport


# -- Project information -----------------------------------------------------
project   = 'LaTeX Wargame exporter'
copyright = '2022, Christian Holm Christensen'
author    = 'Christian Holm Christensen'
version   = '0.1'
release   = '0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx_automodapi.automodapi'
]
templates_path   = ['.templates']
language         = 'English'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
html_theme       = 'alabaster'
html_static_path = ['.static']
master_doc       = 'index'

# -- Extension configuration -------------------------------------------------
builddir                = os.environ['BUILDDIR']
automodapi_toctreedirnm = ((builddir+'/') if builddir != '' else '.') + 'api'
autoclass_content       = 'both'
autodoc_class_signature = 'separated'
