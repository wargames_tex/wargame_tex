#
#
#
NAME		:= wargame
VERSION		:= 0.9
LATEX_FLAGS	:= -interaction=nonstopmode 	\
		   -file-line-error		\
		   --synctex=15			\
		   -shell-escape
LATEX		:= pdflatex  
MAKEINDEX	:= makeindex
SED		:= sed
CTANOMAT	:= ctan-o-mat
CTANOMAT_FLAGS	:= -n
SOURCES		:= wargame.ins						\
		   wargame.dtx						\
		   package.dtx						\
		   util/core.dtx					\
		   util/misc.dtx					\
		   util/compound.dtx					\
		   util/bb.dtx						\
		   util/tikz.dtx					\
		   util/randomid.dtx					\
		   util/icons.dtx					\
		   util/export.dtx					\
		   chit/shape.dtx					\
		   chit/misc.dtx					\
		   chit/modifiers.dtx					\
		   chit/stack.dtx					\
		   chit/oob.dtx						\
		   chit/table.dtx					\
		   chit/battle.dtx					\
		   chit/dice.dtx					\
		   chit/elements.dtx					\
		   chit/core.dtx					\
		   hex/shape.dtx					\
		   hex/tile.dtx						\
		   hex/ridges.dtx					\
		   hex/towns.dtx					\
		   hex/terrain.dtx					\
		   hex/paths.dtx					\
		   hex/core.dtx						\
		   hex/labels.dtx					\
		   hex/extra.dtx					\
		   hex/terrain/woods.dtx				\
		   hex/terrain/town.dtx					\
		   hex/terrain/tree.dtx					\
		   hex/terrain/beach.dtx				\
		   hex/terrain/mountain.dtx				\
		   hex/terrain/light_woods.dtx				\
		   hex/terrain/mountains.dtx				\
		   hex/terrain/village.dtx				\
		   hex/terrain/city.dtx					\
		   hex/terrain/swamp.dtx				\
		   hex/terrain/rough.dtx				\
		   hex/terrain/fields.dtx				\
		   hex/terrain/speckle.dtx				\
		   hex/board.dtx					\
		   hex/coord.dtx					\
		   hex/split.dtx					\
		   natoapp6c/shape.dtx					\
		   natoapp6c/symbols.dtx				\
		   natoapp6c/list.dtx					\
		   natoapp6c/compat/seasurface.dtx			\
		   natoapp6c/compat/activity.dtx			\
		   natoapp6c/compat/subsurface.dtx			\
		   natoapp6c/compat/missile.dtx				\
		   natoapp6c/compat/air.dtx				\
		   natoapp6c/compat/seamine.dtx				\
		   natoapp6c/compat/land.dtx				\
		   natoapp6c/compat/equipment.dtx			\
		   natoapp6c/compat/installation.dtx			\
		   natoapp6c/compat/space.dtx				\
		   natoapp6c/frames/hostile.dtx				\
		   natoapp6c/frames/base.dtx				\
		   natoapp6c/frames/friendly.dtx			\
		   natoapp6c/frames/neutral.dtx				\
		   natoapp6c/frames/unknown.dtx				\
		   natoapp6c/weaponry.dtx				\
		   natoapp6c/core.dtx					\
		   natoapp6c/text.dtx					\
		   natoapp6c/echelon.dtx				\
		   natoapp6c/util.dtx					\
		   silhouette/core.dtx  				\
		   silhouette/misc.dtx					\
		   silhouette/modern.dtx				\
		   silhouette/modern/pl_pt91.dtx			\
		   silhouette/modern/ru_t14.dtx				\
		   silhouette/modern/ua_t84.dtx				\
		   silhouette/modern/us_apache.dtx			\
		   silhouette/modern/us_m1_abrams.dtx			\
		   silhouette/modern/us_sniper_stand.dtx		\
		   silhouette/modern/us_soldier_aim.dtx			\
		   silhouette/modern/us_soldier_front.dtx		\
		   silhouette/modern/us_soldier_kneeling.dtx		\
		   silhouette/modern/us_soldier_look.dtx		\
		   silhouette/modern/us_soldier_running.dtx		\
		   silhouette/modern/us_soldier_side.dtx		\
		   silhouette/modern/us_soldier_stand.dtx		\
		   silhouette/wwii.dtx  				\
		   silhouette/wwii/de_german_shepard.dtx		\
		   silhouette/wwii/de_marder.dtx			\
		   silhouette/wwii/de_mercedes_3000.dtx			\
		   silhouette/wwii/de_messerschmidt.dtx			\
		   silhouette/wwii/de_officer.dtx			\
		   silhouette/wwii/de_opel_blitz.dtx			\
		   silhouette/wwii/de_panzer_i.dtx			\
		   silhouette/wwii/de_panzer_ii.dtx			\
		   silhouette/wwii/de_panzer_iii.dtx			\
		   silhouette/wwii/de_panzer_iv_a.dtx			\
		   silhouette/wwii/de_panzer_iv_h.dtx			\
		   silhouette/wwii/de_panzer_vi_tiger.dtx		\
		   silhouette/wwii/de_panzer_v_panther.dtx		\
		   silhouette/wwii/de_sdkfz_251.dtx			\
		   silhouette/wwii/de_soldier_bayonet.dtx		\
		   silhouette/wwii/de_soldier_down.dtx			\
		   silhouette/wwii/de_soldier_guard.dtx			\
		   silhouette/wwii/de_soldier_side.dtx			\
		   silhouette/wwii/de_soldier_standing.dtx		\
		   silhouette/wwii/de_soldier_up.dtx			\
		   silhouette/wwii/de_soldier_walking.dtx		\
		   silhouette/wwii/fr_amr_33.dtx			\
		   silhouette/wwii/fr_char_b1.dtx			\
		   silhouette/wwii/fr_citroen_23_mle.dtx		\
		   silhouette/wwii/fr_fcm_36.dtx			\
		   silhouette/wwii/fr_renault_ft.dtx			\
		   silhouette/wwii/jp_89_chi_ha.dtx			\
		   silhouette/wwii/jp_89_chi_ro.dtx			\
		   silhouette/wwii/jp_89_i_go.dtx			\
		   silhouette/wwii/jp_94_lorry.dtx			\
		   silhouette/wwii/pl_7tp.dtx				\
		   silhouette/wwii/pl_tk_3.dtx				\
		   silhouette/wwii/su_gaz_aa.dtx			\
		   silhouette/wwii/su_is_2.dtx				\
		   silhouette/wwii/su_kv_1.dtx				\
		   silhouette/wwii/su_soldier_crouch.dtx		\
		   silhouette/wwii/su_soldier_leaning.dtx		\
		   silhouette/wwii/su_soldier_machine_gun.dtx		\
		   silhouette/wwii/su_soldier_running.dtx		\
		   silhouette/wwii/su_su_152.dtx			\
		   silhouette/wwii/su_su_76.dtx				\
		   silhouette/wwii/su_t34_76_alt.dtx			\
		   silhouette/wwii/su_t34_76.dtx			\
		   silhouette/wwii/su_t34_85_alt.dtx			\
		   silhouette/wwii/su_t34_85.dtx			\
		   silhouette/wwii/uk_a34_comet.dtx			\
		   silhouette/wwii/uk_a9_cruiser.dtx			\
		   silhouette/wwii/uk_bren_carrier.dtx			\
		   silhouette/wwii/uk_crusader.dtx			\
		   silhouette/wwii/uk_lancaster.dtx			\
		   silhouette/wwii/uk_m2_churchill.dtx			\
		   silhouette/wwii/uk_m34_caffee.dtx			\
		   silhouette/wwii/uk_m3_cromwell.dtx			\
		   silhouette/wwii/uk_matilda.dtx			\
		   silhouette/wwii/uk_soldier_crouching.dtx		\
		   silhouette/wwii/uk_soldier_grenade.dtx		\
		   silhouette/wwii/uk_soldier_kneeling.dtx		\
		   silhouette/wwii/uk_soldier_running.dtx		\
		   silhouette/wwii/uk_soldier_standing.dtx		\
		   silhouette/wwii/uk_spitfire.dtx			\
		   silhouette/wwii/uk_valentine.dtx			\
		   silhouette/wwii/uk_vickers_mark_e.dtx		\
		   silhouette/wwii/us_gmc_cckw.dtx			\
		   silhouette/wwii/us_grant.dtx				\
		   silhouette/wwii/us_m24_caffee.dtx			\
		   silhouette/wwii/us_m36.dtx				\
		   silhouette/wwii/us_sherman.dtx			\
		   silhouette/wwii/us_soldier_kneeling.dtx		\
		   silhouette/wwii/us_soldier_standing.dtx		\
		   silhouette/wwii/us_soldier_walking_down.dtx		\
		   silhouette/wwii/us_soldier_walking.dtx		\
		   silhouette/wwii/us_soldier_walking_shoulder.dtx	\
		   silhouette/wwii/us_soldier_walking_up.dtx		\
		   silhouette/wwii/us_stuart.dtx			\
		   tests/map.dtx					\
		   tests/chits.dtx			
SCRIPTS		:= utils/wgsvg2tikz.py 					\
		   utils/wgexport.py					\
		   utils/wgmakenato.py					\
		   utils/wgcsv2chits.py					\
		   utils/wgcsv2hexes.py					

DESTDIR		:= $(HOME)/texmf/
instdir		:= tex/latex/$(NAME)
docdir		:= doc/latex/$(NAME)
srcdir		:= source/latex/$(NAME)
ctandir		:= ctan
distsdir	:= $(NAME)-$(VERSION)

TILES		:= beach	\
		   city		\
		   light_woods	\
		   mountains	\
		   rough	\
		   swamp	\
		   town		\
		   village	\
		   woods	\
		   fields	\
		   speckle
TILES_PDF	:= $(TILES:%=wargame.%.pdf)
TILES_TEX	:= $(TILES:%=wargame.%.tex)
TABLES		:= air		\
		   missile	\
		   land		\
		   equipment	\
		   installation	\
		   seasurface	\
		   subsurface	\
		   seamine	\
		   space	\
		   activity
PKG_FILES	:= wargame.sty					\
		   tikzlibrarywargame.util.code.tex		\
		   tikzlibrarywargame.hex.code.tex		\
		   tikzlibrarywargame.natoapp6c.code.tex	\
		   tikzlibrarywargame.chit.code.tex		\
		   tikzlibrarywargame.silhouette.code.tex	\
		   wgexport.cls					\
		   $(TILES_PDF)				

DOC_FILES	:= wargame.pdf symbols.pdf compat.pdf

ifdef VERBOSE	
MUTE		:=
REDIR		:=
LATEX_FLAGS	:= 
else
MUTE		:= @
REDIR		:= > /dev/null 2>&1 
endif
ifdef CI_COMMIT_REF_NAME
VERSION		:= $(CI_COMMIT_REF_NAME)
else
ifdef 		:= $(CI_JOB_ID)
endif

%.pdf:%.tex
	@echo "LATEX $< -> $@"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $<  $(REDIR)

%.aux:%.tex
	@echo "LATEX $< -> $@"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $<  $(REDIR)

%.idx:%.dtx
	@echo "LATEX $< -> $@"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $<  $(REDIR)

%.aux:%.idx
	@echo "LATEX $*.dtx -> $@ (via $<)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $*.dtx  $(REDIR)
	$(MUTE)touch $< $@

%.pdf:%.aux %.ind
	@echo "LATEX $*.dtx -> $@ (via $<)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $*.dtx  $(REDIR)
	$(MUTE)touch $^ $@

%.pdf:%.aux %.tex
	@echo "LATEX $*.tex -> $@ (via $<)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $*.tex  $(REDIR)


%.ind:%.idx
	@echo "INDEX $< -> $@ $<"
	$(MUTE)$(MAKEINDEX) -s gind -o $@ $<  $(REDIR)
	$(MUTE)touch $^ $@

%.gls:%.glo
	@echo "GLOSSAY $< -> $@"
	$(MUTE)$(MAKEINDEX) -s gglo -o $@ $<  $(REDIR)
	$(MUTE)touch $^ $@

all:		wargame.pdf

package:	$(PKG_FILES)

everything:	all symbols.pdf compat.pdf tests/test.pdf

tutorial: tutorial/game.pdf
tutorial/game.pdf: all
	@echo "MAKE -C tutorial Game.vmod"
	$(MUTE)$(MAKE) -C tutorial Game.vmod

clean:
	@echo "CLEAN"
	$(MUTE)rm -f  *~ *.log* *.aux *.toc *.lof *.lot *.out *.ilg *.idx *.ind
	$(MUTE)rm -f  cmp_*.tex *.synctex* *.hd test*.pdf
	$(MUTE)rm -f  symbols.tex compat.tex testmap.tex testchits.tex 
	$(MUTE)rm -f  wgdoc.sty logo.png logo.pdf
	$(MUTE)rm -f  symbols.pdf compat.pdf test.pdf wargame.pdf
	$(MUTE)rm -f  mksvg.pdf README.md.version
	$(MUTE)rm -f  $(TILES_TEX) $(TILES_PDF) $(PKG_FILES)
	$(MUTE)rm -rf public texmf tex doc
	$(MUTE)rm -rf $(distsdir)
	$(MUTE)rm -f  $(distsdir).tar.gz
	$(MUTE)rm -f  $(distsdir).zip
	$(MUTE)rm -f  $(NAME).tar.gz
	$(MUTE)rm -rf __pycache__
	$(MUTE)rm -rf $(ctandir) tmp
	$(MUTE)rm -f  ctan.tex $(NAME).ctan.tar.gz
	$(MUTE)rm -f  tests/*.log tests/*.aux tests/*.out tests/*.pdf
	$(MUTE)rm -f  tests/*.synctex* tests/*.json
	$(MUTE)rm -rf tutorial/vmod
	$(MUTE)$(MAKE) -C tutorial clean 



wargame.sty 				\
testmap.tex 				\
testchits.tex 				\
wgdoc.sty 				\
compat.tex 				\
symbols.tex 				\
tikzlibrarywargame.hex.code.tex		\
tikzlibrarywargame.natoapp6c.code.tex	\
tikzlibrarywargame.chit.code.tex	\
wgexport.cls				\
$(TILES_TEX)		\
$(TABLES:%=cmp_%.tex): $(SOURCES)
	@echo "DOCSTRIP $<"
	$(MUTE)$(LATEX) $< $(REDIR)

wargame.idx:	wargame.dtx wargame.sty $(TILES_PDF)
wargame.pdf:	wargame.aux wargame.ind
fast:		wargame.idx
symbols.aux:	symbols.tex wargame.sty
symbols.pdf:	symbols.aux
compat.aux:	compat.tex $(TABLES:%=cmp_%.tex) wargame.sty
compat.pdf:	compat.aux
test.aux:	test.tex wargame.sty

beach.pdf:	beach.tex
city.pdf:	city.tex
light_woods.pdf:light_woods.tex
mountains.pdf:	mountains.tex
rough.pdf:	rough.tex
swamp.pdf:	swamp.tex
town.pdf:	town.tex
village.pdf:	village.tex
woods.pdf:	woods.tex

install-devel: all 
	(cd $(dir $(DESTDIR)/$(instdir)) && \
		rm -f $(notdir $(instdir)) && \
		ln -fs $(PWD) $(notdir $(instdir)))

install:$(PKG_FILES) $(DOC_FILES) $(SCRIPTS)
	@echo "Installing DESTDIR=$(DESTDIR)"
	$(MUTE)mkdir -p $(DESTDIR)$(instdir)
	$(MUTE)mkdir -p $(DESTDIR)$(docdir)
	$(MUTE)cp $(PKG_FILES) $(DESTDIR)$(instdir)
	$(MUTE)cp $(DOC_FILES) $(DESTDIR)$(docdir)
	$(MUTE)cp $(SCRIPTS)   $(DESTDIR)$(instdir)

uninstall:
	rm -rf $(DESTDIR)$(instdir)
	rm -rf $(DESTDIR)$(docdir)

distdir:$(SOURCES) $(SCRIPTS) Makefile tests/test.tex
	$(MUTE)rm -rf $(distsdir)
	$(MUTE)mkdir -p $(distsdir)
	$(foreach s, $^, 	\
		mkdir -p $(distsdir)/$(shell dirname $(s)); \
		cp $(s) $(distsdir)/$(s);)

distcheck:distdir
	$(MAKE) $(MAKEFLAGS) -C $(distsdir) everything

dist:distdir
	zip -r $(distsdir).zip $(distsdir)

bindist:	DESTDIR:=$(distsdir)/
bindist:install
	zip -r $(distsdir).zip $(distsdir)


logo.png:logo.pdf
	pdftocairo -transp -png $<
	mv logo-1.png logo.png

README.md.version:README.md
	@echo "Versioned README.md $(VERSION)"
	$(MUTE)$(SED) 's/^# \(.*\)/# \1\n## Version $(VERSION)/' \
		< $< | pandoc --to markdown > $@

ctandir: all tutorial/game.pdf README.md.version
	@echo "Preparing CTAN directory"
	$(MUTE)$(MAKE) install DESTDIR:=tmp/ 
	$(MUTE)mkdir -p tmp/$(docdir)/tutorial
	$(MUTE)cp $(foreach i, game.tex game.sty export.tex patch.py \
	                README.md Makefile game.pdf,\
		tutorial/$(i)) tmp/$(docdir)/tutorial
	$(MUTE)cp README.md.version tmp/$(docdir)/README.md
	$(MUTE)$(MAKE) distdir distsdir:=tmp/$(srcdir) $(REDIR)
	$(MUTE)mkdir -p $(ctandir)/$(NAME)
	$(MUTE)cp tmp/$(docdir)/README.md $(ctandir)/$(NAME)/
	$(MUTE)(cd tmp && zip -q -r ../$(ctandir)/$(NAME).tds.zip *)
	$(MUTE)rm tmp/$(docdir)/README.md
	$(MUTE)(cd tmp && cp -a $(docdir) ../$(ctandir)/$(NAME)/doc)
	$(MUTE)(cd tmp && cp -a $(srcdir) ../$(ctandir)/$(NAME)/source)
	$(MUTE) rm -rf tmp

ctan.tex:utils/ctan.tex.in 
	@echo "SED	$< -> $@"
	$(MUTE)$(SED)   -e 's/@NAME@/$(NAME)/g' \
			-e 's/@VERSION@/$(VERSION)/' \
		< $< > $@

ctandist: $(NAME).ctan.tar.gz
$(NAME).ctan.tar.gz:	ctandir
	@echo "TAR	$@"
	$(MUTE)rm -f $@
	$(MUTE)(cd $(ctandir) && tar -czf ../$(NAME).ctan.tar.gz *)
	$(MUTE)rm -rf $(ctandir) tmp


ctan-upload:ctan.tex $(NAME).ctan.tar.gz  
	@echo "CTAN	$< -> CTAN ($(CTANOMAT_FLAGS))"
	$(MUTE)$(CTANOMAT) $(CTANOMAT_FLAGS) -c $<


docker:
	docker run --user root --group-add users -e GRANT_SUDO=yes -it --rm \
		-v $(PWD):/root/$(notdir $(PWD)) texlive/texlive \
		/bin/bash

docker-prep:
	apt update
	apt install -y poppler-utils python3-pil pandoc zip \
		libwww-mechanize-perl libfile-copy-recursive-perl
	pwd 
	ls
	make clean

docker-artifacts: DESTDIR=$(PWD)/
docker-artifacts: install tutorial/game.pdf
	cp tutorial/game.pdf  $(DESTDIR)$(docdir)/tutorial.pdf
	cp tutorial/Game.vmod $(DESTDIR)$(docdir)/Game.vmod


#
# EOF
#
